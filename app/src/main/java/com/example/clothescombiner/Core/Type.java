package com.example.clothescombiner.Core;

import androidx.annotation.NonNull;

public enum Type {
    ELBISE, AYAKKABI, CANTA, GOZLUK, UST, ALT, SAPKA;

    @NonNull
    @Override
    public String toString() {
        switch (this) {
            case ELBISE:
                return "Elbise";
            case AYAKKABI:
                return "Ayakkabı";
            case CANTA:
                return "Çanta";
            case GOZLUK:
                return "Gözlük";
            case UST:
                return "Ust Giyim";
            case ALT:
                return "Alt Giyim";
            case SAPKA:
                return "Şapka";
            default:
                return null;
        }
    }
}
