package com.example.clothescombiner.Core;

import androidx.annotation.NonNull;

public enum Gender {
    MAN, WOMAN;

    @NonNull
    @Override
    public String toString() {
        switch (this) {
            case MAN:
                return "Man";
            case WOMAN:
                return "Women";
            default:
                return null;
        }
    }
}
