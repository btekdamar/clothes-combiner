package com.example.clothescombiner.Core;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class User {
    private String fullName;
    private String email;
    private String userGender;

    public User() {

    }

    public User(String name, String email, String gender) {
        this.fullName = name;
        this.email = email;
        this.userGender = gender;
    }

    public User(User user) {
        this.fullName = user.fullName;
        this.email = user.email;
        this.userGender = user.userGender;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    @Override
    public String toString() {
        return "User{" +
                "fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", userGender=" + userGender +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullName, email, userGender);
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", this.fullName);
        result.put("email", this.email);
        result.put("gender", this.userGender);

        return result;
    }
}
