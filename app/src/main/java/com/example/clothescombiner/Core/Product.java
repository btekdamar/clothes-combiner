package com.example.clothescombiner.Core;

import java.io.Serializable;
import java.util.Objects;

public class Product implements Serializable {
    private String id;
    private String url;
    private String name;
    private String price;
    private String color;
    private String gender;
    private String type;
    private String image;
    private String brand;
    private boolean visible;
    private Type filterType;

    public Product() {

    }

    public Product(String id, String url, String name, String price, String color, String gender, String type, String imageURL, String productBrand, boolean visible, Type filterType) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.price = price;
        this.color = color;
        this.gender = gender;
        this.type = type;
        this.image = imageURL;
        this.brand = productBrand;
        this.visible = visible;
        this.filterType = filterType;
    }

    public Product(Product product) {
        this.id = product.id;
        this.url = product.url;
        this.name = product.name;
        this.price = product.price;
        this.color = product.color;
        this.gender = product.gender;
        this.type = product.type;
        this.image = product.image;
        this.brand = product.brand;
        this.visible = product.visible;
        this.filterType = product.filterType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Type getFilterType() {
        return filterType;
    }

    public void setFilterType(Type filterType) {
        this.filterType = filterType;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", color='" + color + '\'' +
                ", gender='" + gender + '\'' +
                ", type='" + type + '\'' +
                ", image='" + image + '\'' +
                ", brand='" + brand + '\'' +
                ", visible=" + visible +
                ", filterType=" + filterType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return visible == product.visible &&
                Objects.equals(id, product.id) &&
                Objects.equals(url, product.url) &&
                Objects.equals(name, product.name) &&
                Objects.equals(price, product.price) &&
                Objects.equals(color, product.color) &&
                Objects.equals(gender, product.gender) &&
                Objects.equals(type, product.type) &&
                Objects.equals(image, product.image) &&
                Objects.equals(brand, product.brand) &&
                filterType == product.filterType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, url, name, price, color, gender, type, image, brand, visible, filterType);
    }
}
