package com.example.clothescombiner.Core;

import androidx.annotation.NonNull;

public enum Brands {
    MANGO, HM, TWIST, BERSHKA, IPEKYOL;

    @NonNull
    @Override
    public String toString() {
        switch (this) {
            case MANGO:
                return "Mango";
            case HM:
                return "H&M";
            case TWIST:
                return "Twist";
            case BERSHKA:
                return "Bershka";
            case IPEKYOL:
                return "Ipekyol";
            default:
                return null;
        }
    }
}


