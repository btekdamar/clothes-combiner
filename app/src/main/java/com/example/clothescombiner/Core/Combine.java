package com.example.clothescombiner.Core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Combine implements Serializable {
    private String id;
    private String name;
    private String user;
    private double rate;
    private double price;
    private Product hat;
    private Product bag;
    private Product shoes;
    private Product upperGarment;
    private Product bottomGarment;
    private Product dress;
    private Product glasses;
    private boolean visible;
    private ArrayList<FashionType> fashionTypes;

    public Combine() {

    }

    public Combine(String name, String user, double rate,
                   double price, Product upperGarment, Product bottomGarment,
                   Product dress, Product glasses, Product bag, Product hat, Product shoes, boolean visible, ArrayList<FashionType> fashionTypes) {
        this.name = name;
        this.user = user;
        this.rate = rate;
        this.price = price;
        this.upperGarment = upperGarment;
        this.bottomGarment = bottomGarment;
        this.dress = dress;
        this.glasses = glasses;
        this.bag = bag;
        this.hat = hat;
        this.shoes = shoes;
        this.visible = visible;
        this.fashionTypes = fashionTypes;
    }

    public Combine(Combine combine) {
        this.id = combine.id;
        this.name = combine.name;
        this.user = combine.user;
        this.rate = combine.rate;
        this.price = combine.price;
        this.upperGarment = combine.upperGarment;
        this.bottomGarment = combine.bottomGarment;
        this.dress = combine.dress;
        this.glasses = combine.glasses;
        this.bag = combine.bag;
        this.hat = combine.hat;
        this.shoes = combine.shoes;
        this.visible = combine.visible;
        this.fashionTypes = combine.fashionTypes;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Product getHat() {
        return hat;
    }

    public void setHat(Product hat) {
        this.hat = hat;
    }

    public Product getBag() {
        return bag;
    }

    public void setBag(Product bag) {
        this.bag = bag;
    }

    public Product getShoes() {
        return shoes;
    }

    public void setShoes(Product shoes) {
        this.shoes = shoes;
    }

    public Product getUpperGarment() {
        return upperGarment;
    }

    public void setUpperGarment(Product upperGarment) {
        this.upperGarment = upperGarment;
    }

    public Product getBottomGarment() {
        return bottomGarment;
    }

    public void setBottomGarment(Product bottomGarment) {
        this.bottomGarment = bottomGarment;
    }

    public Product getDress() {
        return dress;
    }

    public void setDress(Product dress) {
        this.dress = dress;
    }

    public Product getGlasses() {
        return glasses;
    }

    public void setGlasses(Product glasses) {
        this.glasses = glasses;
    }

    public ArrayList<FashionType> getFashionTypes() {
        return fashionTypes;
    }

    public void setFashionTypes(ArrayList<FashionType> fashionTypes) {
        this.fashionTypes = fashionTypes;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Combine combine = (Combine) o;
        return Double.compare(combine.rate, rate) == 0 &&
                Double.compare(combine.price, price) == 0 &&
                visible == combine.visible &&
                Objects.equals(id, combine.id) &&
                Objects.equals(name, combine.name) &&
                Objects.equals(user, combine.user) &&
                Objects.equals(hat, combine.hat) &&
                Objects.equals(bag, combine.bag) &&
                Objects.equals(shoes, combine.shoes) &&
                Objects.equals(upperGarment, combine.upperGarment) &&
                Objects.equals(bottomGarment, combine.bottomGarment) &&
                Objects.equals(dress, combine.dress) &&
                Objects.equals(glasses, combine.glasses) &&
                Objects.equals(fashionTypes, combine.fashionTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, user, rate, price, hat, bag, shoes, upperGarment, bottomGarment, dress, glasses, visible, fashionTypes);
    }

    @Override
    public String toString() {
        return "Combine{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", user='" + user + '\'' +
                ", rate=" + rate +
                ", price=" + price +
                ", hat=" + hat +
                ", bag=" + bag +
                ", shoes=" + shoes +
                ", upperGarment=" + upperGarment +
                ", bottomGarment=" + bottomGarment +
                ", dress=" + dress +
                ", glasses=" + glasses +
                ", visible=" + visible +
                ", fashionTypes=" + fashionTypes +
                '}';
    }

    public void calculatePrice() {
        this.price = 0;

        if (this.hat != null)
            this.price += parsePrice(this.hat.getPrice());
        if (this.bag != null)
            this.price += parsePrice(this.bag.getPrice());
        if (this.dress != null)
            this.price += parsePrice(this.dress.getPrice());
        if (this.upperGarment != null)
            this.price += parsePrice(this.upperGarment.getPrice());
        if (this.bottomGarment != null)
            this.price += parsePrice(this.bottomGarment.getPrice());
        if (this.shoes != null)
            this.price += parsePrice(this.shoes.getPrice());
        if (this.glasses != null)
            this.price += parsePrice(this.glasses.getPrice());
    }

    private double parsePrice(String price) {
        price = price.replace(',', '.');
        price = price.replace('₺', ' ');
        price = price.replace("TL", " ");
        return Double.parseDouble(price);
    }

}
