package com.example.clothescombiner.Core;

import androidx.annotation.NonNull;

public enum BottomNav {
    HOME, MY, PRODUCTS;

    @NonNull
    @Override
    public String toString() {
        switch (this) {
            case HOME:
                return "Home";
            case MY:
                return "MyCombines";
            case PRODUCTS:
                return "Products";
            default:
                return null;
        }
    }
}
