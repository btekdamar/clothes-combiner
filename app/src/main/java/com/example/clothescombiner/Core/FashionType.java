package com.example.clothescombiner.Core;

import androidx.annotation.NonNull;

public enum FashionType {
    BUSINESS, WEEKEND, DINNER, SUMMER, WINTER, FALL, CASUAL;

    @NonNull
    @Override
    public String toString() {
        switch (this) {
            case BUSINESS:
                return "Business";
            case WEEKEND:
                return "Weekend";
            case DINNER:
                return "Dinner";
            case SUMMER:
                return "Summer";
            case WINTER:
                return "Winter";
            case FALL:
                return "Fall";
            case CASUAL:
                return "Casual";
            default:
                return null;
        }
    }
}
