package com.example.clothescombiner.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clothescombiner.Activities.EditCombine;
import com.example.clothescombiner.Activities.MainActivity;
import com.example.clothescombiner.Core.Combine;
import com.example.clothescombiner.Core.User;
import com.example.clothescombiner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

import static com.example.clothescombiner.Activities.MainActivity.getCurrentUser;

public class MyCombinesAdapter extends RecyclerView.Adapter<MyCombinesAdapter.ViewHolder> {
    private static final String TAG = "MyCombinesAdapter";
    private static final String USERS = "Users";
    private static final String COMBINES = "Combines";
    private static final String myCOMBINES = "MyCombines";
    private final ArrayList<Combine> dataSet;
    private final Context mContext;
    private final OnCombineListener mOnCombineListener;
    private final DatabaseReference mDatabaseReference;
    private final FirebaseAuth mAuth;
    private final User user;

    public MyCombinesAdapter(Context context, ArrayList<Combine> dataSet, OnCombineListener onCombineListener, boolean isFirst) {
        this.mContext = context;
        this.dataSet = dataSet;
        if (isFirst)
            Collections.reverse(this.dataSet);
        this.mOnCombineListener = onCombineListener;
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mDatabase.getReference();
        mAuth = FirebaseAuth.getInstance();
        user = getCurrentUser();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder called");
        LayoutInflater inflater = LayoutInflater.from(this.mContext);
        View view = inflater.inflate(R.layout.my_combines_row, parent, false);
        return new ViewHolder(view, mOnCombineListener);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder called");
        final Combine selectedCombine = dataSet.get(position);

        holder.dress.setImageBitmap(null);
        holder.upper.setImageBitmap(null);
        holder.bottom.setImageBitmap(null);
        holder.bag.setImageBitmap(null);
        holder.shoes.setImageBitmap(null);
        holder.glasses.setImageBitmap(null);
        holder.hat.setImageBitmap(null);

        holder.combineName.setText(selectedCombine.getName());

        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        holder.combineRateValue.setText((df.format(selectedCombine.getRate())));
        df = new DecimalFormat("#.##");
        holder.price.setText(df.format(selectedCombine.getPrice()) + " TL");
        holder.combineRate.setRating((float) selectedCombine.getRate());
        holder.switchOnOff.setChecked(selectedCombine.isVisible());

        if (selectedCombine.getDress() != null) {
            holder.upper.setVisibility(View.INVISIBLE);
            holder.bottom.setVisibility(View.INVISIBLE);
            holder.dress.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(selectedCombine.getDress().getImage())
                    .resize(150, 300)
                    .into(holder.dress);
        }

        if (selectedCombine.getUpperGarment() != null) {
            holder.upper.setVisibility(View.VISIBLE);
            holder.dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(selectedCombine.getUpperGarment().getImage())
                    .into(holder.upper);
        }

        if (selectedCombine.getBottomGarment() != null) {
            holder.bottom.setVisibility(View.VISIBLE);
            holder.dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(selectedCombine.getBottomGarment().getImage())
                    .into(holder.bottom);
        }

        if (selectedCombine.getHat() != null) {
            Picasso.get()
                    .load(selectedCombine.getHat().getImage())
                    .into(holder.hat);
        }

        if (selectedCombine.getShoes() != null) {
            Picasso.get()
                    .load(selectedCombine.getShoes().getImage())
                    .into(holder.shoes);
        }

        if (selectedCombine.getBag() != null) {
            Picasso.get()
                    .load(selectedCombine.getBag().getImage())
                    .into(holder.bag);
        }

        if (selectedCombine.getGlasses() != null) {
            Picasso.get()
                    .load(selectedCombine.getGlasses().getImage())
                    .into(holder.glasses);
        }

        holder.switchOnOff.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!user.getFullName().equals(selectedCombine.getUser())) {
                Toast.makeText(mContext, "It's not your combine!!! So you can not change visibility", Toast.LENGTH_SHORT).show();
                holder.switchOnOff.setChecked(selectedCombine.isVisible());
            } else {
                if (holder.switchOnOff.isChecked()) {
                    String userId = mAuth.getUid();
                    assert userId != null;
                    mDatabaseReference.child(USERS).child(userId).child(myCOMBINES)
                            .child(String.valueOf(selectedCombine.getId())).child("visible").setValue(true);

                    mDatabaseReference.child(COMBINES).child(String.valueOf(selectedCombine
                            .getId())).child("visible").setValue(true);

                } else {
                    String userId = mAuth.getUid();
                    assert userId != null;
                    mDatabaseReference.child(USERS).child(userId).child(myCOMBINES)
                            .child(String.valueOf(selectedCombine.getId())).child("visible").setValue(false);

                    mDatabaseReference.child(COMBINES).child(String.valueOf(selectedCombine
                            .getId())).child("visible").setValue(false);
                }
            }
        });

        holder.switchOnOff.setOnClickListener(v -> {
            if (!user.getFullName().equals(selectedCombine.getUser())) {
                Toast.makeText(mContext, "It's not your combine!!! So you can not change visibility", Toast.LENGTH_SHORT).show();
                holder.switchOnOff.setChecked(selectedCombine.isVisible());
            } else {
                if (holder.switchOnOff.isChecked()) {
                    String userId = mAuth.getUid();
                    assert userId != null;
                    mDatabaseReference.child(USERS).child(userId).child(myCOMBINES)
                            .child(String.valueOf(selectedCombine.getId())).child("visible").setValue(true);

                    mDatabaseReference.child(COMBINES).child(String.valueOf(selectedCombine
                            .getId())).child("visible").setValue(true);

                } else {
                    String userId = mAuth.getUid();
                    assert userId != null;
                    mDatabaseReference.child(USERS).child(userId).child(myCOMBINES)
                            .child(String.valueOf(selectedCombine.getId())).child("visible").setValue(false);

                    mDatabaseReference.child(COMBINES).child(String.valueOf(selectedCombine
                            .getId())).child("visible").setValue(false);
                }
            }
        });

        holder.combineDelete.setOnClickListener(v -> {
            Log.d(TAG, "onClick: deleteCombine");
            new AlertDialog.Builder(mContext)
                    .setTitle("Are you sure?")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        String userId = mAuth.getUid();
                        assert userId != null;
                        mDatabaseReference.child(COMBINES).child(selectedCombine.getId())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if (snapshot.exists()) {
                                            Combine temp = snapshot.getValue(Combine.class);
                                            assert temp != null;
                                            if (temp.getUser().equals(user.getFullName()))
                                                mDatabaseReference.child(COMBINES)
                                                        .child(selectedCombine.getId()).setValue(null);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                        mDatabaseReference.child(USERS).child(userId)
                                .child(myCOMBINES).child(selectedCombine.getId()).setValue(null);
                        notifyItemRemoved(position);
                        dataSet.remove(selectedCombine);
                    })
                    .setNegativeButton("No", (dialog, which) -> dialog.dismiss()).show();
        });

        holder.combineEdit.setOnClickListener(v -> {
            Log.d(TAG, "onClick: combineEdit");
            Intent myIntent = new Intent(mContext, EditCombine.class);
            myIntent.putExtra("Combine", selectedCombine);
            mContext.startActivity(myIntent);
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView combineName;
        private final TextView price;
        private final TextView combineRateValue;
        private final ImageView hat, upper, bottom, dress, shoes, glasses, bag;
        private final RatingBar combineRate;
        private final ImageButton combineDelete;
        private final ImageButton combineEdit;
        private final SwitchCompat switchOnOff;
        private final OnCombineListener onCombineListener;

        public ViewHolder(@NonNull View itemView, OnCombineListener onCombineListener) {
            super(itemView);
            combineName = itemView.findViewById(R.id.text_combinename);
            combineRate = itemView.findViewById(R.id.rating_rate);
            combineRateValue = itemView.findViewById(R.id.text_rate);
            price = itemView.findViewById(R.id.text_pricevalue);
            hat = itemView.findViewById(R.id.image_hat);
            upper = itemView.findViewById(R.id.image_upperGarment);
            bottom = itemView.findViewById(R.id.image_bottomGarment);
            dress = itemView.findViewById(R.id.image_dress);
            shoes = itemView.findViewById(R.id.image_shoes);
            bag = itemView.findViewById(R.id.image_bag);
            glasses = itemView.findViewById(R.id.image_glasses);
            switchOnOff = itemView.findViewById(R.id.switch_my_combine);
            this.onCombineListener = onCombineListener;
            itemView.setOnClickListener(this);
            combineDelete = itemView.findViewById(R.id.image_delete);
            combineEdit = itemView.findViewById(R.id.image_edit);
        }

        @Override
        public void onClick(View v) {
            onCombineListener.onCombineClick(getAdapterPosition());
        }
    }

    public interface OnCombineListener {
        void onCombineClick(int position);
    }
}
