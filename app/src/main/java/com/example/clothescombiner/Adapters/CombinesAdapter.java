package com.example.clothescombiner.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clothescombiner.Activities.MainActivity;
import com.example.clothescombiner.Core.Combine;
import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.R;
import com.squareup.picasso.Picasso;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

import static com.example.clothescombiner.Activities.MainActivity.myWardrobe;

public class CombinesAdapter extends RecyclerView.Adapter<CombinesAdapter.ViewHolder> {
    private static final String TAG = "CombinesAdapter";
    private final ArrayList<Combine> dataSet;
    private final Context mContext;
    private final OnCombineListener mOnCombineListener;

    public CombinesAdapter(Context context, ArrayList<Combine> dataSet, OnCombineListener onCombineListener, boolean isFirst) {
        this.mContext = context;
        MainActivity.MergeSort ms = new MainActivity.MergeSort(dataSet);
        ms.sortGivenArray();
        this.dataSet = ms.getSortedArray();
        Collections.reverse(this.dataSet);
        this.mOnCombineListener = onCombineListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder called");
        LayoutInflater inflater = LayoutInflater.from(this.mContext);
        View view = inflater.inflate(R.layout.combines_row, parent, false);
        return new ViewHolder(view, mOnCombineListener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder called");
        final Combine selectedCombine = dataSet.get(position);
        String productList = "";

        holder.dress.setImageBitmap(null);
        holder.upper.setImageBitmap(null);
        holder.bottom.setImageBitmap(null);
        holder.bag.setImageBitmap(null);
        holder.shoes.setImageBitmap(null);
        holder.glasses.setImageBitmap(null);
        holder.hat.setImageBitmap(null);

        holder.combineName.setText(selectedCombine.getName() + ", ");

        holder.userName.setText(selectedCombine.getUser());
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        holder.combineRateValue.setText((df.format(selectedCombine.getRate())));
        df = new DecimalFormat("#.##");
        holder.price.setText(df.format(selectedCombine.getPrice()) + " TL");
        holder.combineRate.setRating((float) selectedCombine.getRate());

        if (selectedCombine.getDress() != null) {
            holder.upper.setVisibility(View.INVISIBLE);
            holder.bottom.setVisibility(View.INVISIBLE);
            holder.dress.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(selectedCombine.getDress().getImage())
                    .resize(150, 300)
                    .into(holder.dress);
        }

        if (selectedCombine.getUpperGarment() != null) {
            holder.upper.setVisibility(View.VISIBLE);
            holder.dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(selectedCombine.getUpperGarment().getImage())
                    .into(holder.upper);
        }

        if (selectedCombine.getBottomGarment() != null) {
            holder.bottom.setVisibility(View.VISIBLE);
            holder.dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(selectedCombine.getBottomGarment().getImage())
                    .into(holder.bottom);
        }

        if (selectedCombine.getHat() != null) {
            Picasso.get()
                    .load(selectedCombine.getHat().getImage())
                    .into(holder.hat);
        }

        if (selectedCombine.getShoes() != null) {
            Picasso.get()
                    .load(selectedCombine.getShoes().getImage())
                    .into(holder.shoes);
        }

        if (selectedCombine.getBag() != null) {
            Picasso.get()
                    .load(selectedCombine.getBag().getImage())
                    .into(holder.bag);
        }

        if (selectedCombine.getGlasses() != null) {
            Picasso.get()
                    .load(selectedCombine.getGlasses().getImage())
                    .into(holder.glasses);
        }

        for (Product p : myWardrobe) {
            if (!p.isVisible()) {
                continue;
            } else if (selectedCombine.getBag() != null && p.getId().equals(selectedCombine.getBag().getId())) {
                productList += (p.getName()) + ("\n");
            } else if (selectedCombine.getBottomGarment() != null && p.getId().equals(selectedCombine.getBottomGarment().getId())) {
                productList += (p.getName()) + ("\n");
            } else if (selectedCombine.getDress() != null && p.getId().equals(selectedCombine.getDress().getId())) {
                productList += (p.getName()) + ("\n");
            } else if (selectedCombine.getGlasses() != null && p.getId().equals(selectedCombine.getGlasses().getId())) {
                productList += (p.getName()) + ("\n");
            } else if (selectedCombine.getShoes() != null && p.getId().equals(selectedCombine.getShoes().getId())) {
                productList += (p.getName()) + ("\n");
            } else if (selectedCombine.getUpperGarment() != null && p.getId().equals(selectedCombine.getUpperGarment().getId())) {
                productList += (p.getName()) + ("\n");
            } else if (selectedCombine.getHat() != null && p.getId().equals(selectedCombine.getHat().getId())){
                productList += (p.getName()) + ("\n");
            }
        }

        if (productList.equals("")) {
            holder.listTag.setVisibility(View.GONE);
            holder.wardrobeList.setVisibility(View.GONE);
        } else {
            holder.listTag.setVisibility(View.VISIBLE);
            holder.wardrobeList.setVisibility(View.VISIBLE);
            holder.wardrobeList.setText(productList);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public interface OnCombineListener {
        void onCombineClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView combineName;
        private final TextView userName;
        private final TextView price;
        private final TextView combineRateValue;
        private final TextView wardrobeList;
        private final TextView listTag;
        private final ImageView hat, upper, bottom, dress, shoes, glasses, bag;
        private final RatingBar combineRate;
        private final OnCombineListener onCombineListener;

        public ViewHolder(@NonNull View itemView, OnCombineListener onCombineListener) {
            super(itemView);
            combineName = itemView.findViewById(R.id.text_combinename);
            userName = itemView.findViewById(R.id.text_combineuser);
            combineRate = itemView.findViewById(R.id.rating_rate);
            combineRateValue = itemView.findViewById(R.id.text_rate);
            price = itemView.findViewById(R.id.text_pricevalue);
            hat = itemView.findViewById(R.id.image_hat);
            upper = itemView.findViewById(R.id.image_upperGarment);
            bottom = itemView.findViewById(R.id.image_bottomGarment);
            dress = itemView.findViewById(R.id.image_dress);
            shoes = itemView.findViewById(R.id.image_shoes);
            bag = itemView.findViewById(R.id.image_bag);
            glasses = itemView.findViewById(R.id.image_glasses);
            wardrobeList = itemView.findViewById(R.id.text_wardrobe_list);
            listTag = itemView.findViewById(R.id.text_list);
            this.onCombineListener = onCombineListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCombineListener.onCombineClick(getAdapterPosition());
        }
    }
}
