package com.example.clothescombiner.Adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyWardrobeAdapter extends RecyclerView.Adapter<MyWardrobeAdapter.ViewHolder> {
    private static final String TAG = "My Wardrobe Adapter";
    private static final String USERS = "Users";
    private static final String myWARDROBE = "MyWardrobe";
    private final ArrayList<Product> products;
    private final Context mContext;
    private final OnProductListener mOnProductListener;
    private final DatabaseReference mDatabaseReference;
    private final FirebaseAuth mAuth;


    public MyWardrobeAdapter(Context mContext, ArrayList<Product> products, OnProductListener onProductListener) {
        this.products = products;
        this.mContext = mContext;
        this.mOnProductListener = onProductListener;
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mDatabase.getReference();
        mAuth = FirebaseAuth.getInstance();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder called");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_column, parent, false);
        return new ViewHolder(view, mOnProductListener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder called");
        final Product product = products.get(position);
        Picasso.get()
                .load(product.getImage())
                .into(holder.productImage);
        holder.productName.setText(product.getName());
        holder.productPrice.setText(product.getPrice());
        holder.productBrand.setText(product.getBrand());
        holder.productOnOff.setVisibility(View.VISIBLE);

        holder.productOnOff.setChecked(product.isVisible());

        holder.productOnOff.setOnClickListener(v -> {
            if (holder.productOnOff.isChecked()) {
                String userId = mAuth.getUid();
                assert userId != null;
                mDatabaseReference.child(USERS).child(userId).child(myWARDROBE)
                        .child(String.valueOf(products.get(position).getId())).child("visible").setValue(true);
            } else {
                String userId = mAuth.getUid();
                assert userId != null;
                mDatabaseReference.child(USERS).child(userId).child(myWARDROBE)
                        .child(String.valueOf(products.get(position).getId())).child("visible").setValue(false);
            }
        });

        holder.productDetail.setOnClickListener(v -> {
            Log.d(TAG, "onClick: productDetail");
            final Dialog myDialog = new Dialog(mContext);
            myDialog.setContentView(R.layout.custom_popup);
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            TextView closePopUp = myDialog.findViewById(R.id.text_close);
            ImageView imagePopUp = myDialog.findViewById(R.id.image_popup);
            Picasso.get()
                    .load(product.getImage())
                    .resize(1000, 1499)
                    .into(imagePopUp);
            closePopUp.setOnClickListener(v1 -> myDialog.dismiss());
            myDialog.show();
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView productImage;
        TextView productName;
        TextView productBrand;
        TextView productPrice;
        ImageButton productDetail;
        SwitchCompat productOnOff;
        OnProductListener onProductListener;
        public ViewHolder(@NonNull View itemView, OnProductListener onProductListener) {
            super(itemView);
            productImage = itemView.findViewById(R.id.image_product);
            productName = itemView.findViewById(R.id.text_productname);
            productBrand = itemView.findViewById(R.id.text_productbrand);
            productPrice = itemView.findViewById(R.id.text_productprice);
            productDetail = itemView.findViewById(R.id.image_detail);
            productOnOff = itemView.findViewById(R.id.switch_wardrobe_product);
            this.onProductListener = onProductListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onProductListener.onProductClick(getAdapterPosition());
        }
    }

    public interface OnProductListener {
        void onProductClick(int position);
    }
}
