package com.example.clothescombiner.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.clothescombiner.Adapters.CombinesAdapter;
import com.example.clothescombiner.Core.BottomNav;
import com.example.clothescombiner.Core.Brands;
import com.example.clothescombiner.Core.Combine;
import com.example.clothescombiner.Core.FashionType;
import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.Core.Type;
import com.example.clothescombiner.Core.User;
import com.example.clothescombiner.Fragments.HomeFragment;
import com.example.clothescombiner.Fragments.MyCombinesFragment;
import com.example.clothescombiner.Fragments.ProductsFragment;
import com.example.clothescombiner.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import static com.example.clothescombiner.Activities.SplashScreen.mAuth;
import static com.example.clothescombiner.Activities.SplashScreen.mDatabaseReference;

public class MainActivity extends AppCompatActivity implements CombinesAdapter.OnCombineListener {
    private static final String TAG = "MainActivity";
    private static final String TITLE = "Home";
    private static final String USERS = "Users";
    private static final String COMBINES = "Combines";
    private static final String myCOMBINES = "MyCombines";
    private static final String myWARDROBE = "MyWardrobe";
    private static final String PRODUCTS = "Products";
    protected static User user;

    private RelativeLayout relativeFilter;
    private RelativeLayout relativeMenu;
    private RelativeLayout relativeDrawer;
    private LinearLayout linearLogout;
    private LinearLayout linearLogin;
    private FrameLayout frameLayout;
    private Fragment selectedFragment;
    private TextView textUser;
    private DrawerLayout drawerLayout;
    private BottomNavigationView bottomNav;
    private AppCompatCheckBox mangoCheck, hmCheck, twistCheck, bershkaCheck, ipekyolCheck, myWardrobeCheck,
            businessCheck, weekendCheck, dinnerCheck, summerCheck, winterCheck, fallCheck, casualCheck,
            hatCheck, bagCheck, glassesCheck, shoesCheck, upperCheck, bottomCheck, dressCheck;
    private EditText editMin, editMax;
    private final JSONObject filterJSON = new JSONObject();

    private BottomNav control;

    private ArrayList<Combine> combines = new ArrayList<>();
    private final ArrayList<Combine> myCombines = new ArrayList<>();
    protected static final ArrayList<Product> products = new ArrayList<>();
    public static ArrayList<Product> myWardrobe = new ArrayList<>();

    private double currentRating;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        initializeById();
        currentRating = 0;

        control = BottomNav.HOME;

        bottomNav.setOnNavigationItemSelectedListener(navListener);

        if (mAuth.getCurrentUser() != null) {
            final String userId = mAuth.getUid();
            assert userId != null;
            mDatabaseReference.child(USERS)
                    .child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    MainActivity.user = snapshot.getValue(User.class);
                    linearLogin.setVisibility(View.VISIBLE);
                    linearLogout.setVisibility(View.INVISIBLE);
                    textUser.setText(user.getFullName());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

        myWardrobeCheck.setOnClickListener(v -> {
            if (myWardrobeCheck.isChecked()) {
                mangoCheck.setClickable(false);
                hmCheck.setClickable(false);
                twistCheck.setClickable(false);
                bershkaCheck.setClickable(false);
                ipekyolCheck.setClickable(false);
            } else {
                mangoCheck.setClickable(true);
                hmCheck.setClickable(true);
                twistCheck.setClickable(true);
                bershkaCheck.setClickable(true);
                ipekyolCheck.setClickable(true);
            }
        });

        setHomeList();
        setProductList();
        if (mAuth.getCurrentUser() != null) {
            setUserCombineList();
            setWardrobe();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (user != null) {
            linearLogin.setVisibility(View.VISIBLE);
            linearLogout.setVisibility(View.INVISIBLE);
            textUser.setText(user.getFullName());
        }

        if (user == null) {
            linearLogin.setVisibility(View.INVISIBLE);
            linearLogout.setVisibility(View.VISIBLE);
        }

        if (mAuth.getCurrentUser() != null) {
            final String userId = mAuth.getUid();
            assert userId != null;
            mDatabaseReference.child(USERS).child(userId)
                    .child(myCOMBINES).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        for (DataSnapshot ds : snapshot.getChildren()) {
                            for (Combine combine : combines) {
                                String combineId = combine.getId();
                                if (Objects.equals(ds.getKey(), combineId)) {
                                    mDatabaseReference.child(USERS).child(userId)
                                            .child(myCOMBINES).child(combineId)
                                            .child("rate").setValue(combine.getRate());
                                    break;
                                }
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @SuppressLint("NonConstantResourceId")
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            control = BottomNav.HOME;
                            try {
                                resetFilter();
                                setFilter();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                        case R.id.nav_my_combine:
                            if (mAuth != null && mAuth.getCurrentUser() != null) {
                                control = BottomNav.MY;
                                try {
                                    resetFilter();
                                    setFilter();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case R.id.nav_products:
                            control = BottomNav.PRODUCTS;
                            try {
                                resetFilter();
                                getProductStartScreen();
                                setFilter();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                    return true;
                }
            };

    public void ClickMenu(View view) {
        relativeMenu.bringToFront();
        relativeMenu.setVisibility(View.VISIBLE);
        relativeFilter.setVisibility(View.GONE);
        ViewGroup.LayoutParams params = relativeDrawer.getLayoutParams();
        float density = this.getResources().getDisplayMetrics().density;
        float px = 250 * density;
        params.width = (int) px;
        openDrawer(drawerLayout);
    }

    public void FilterClick(View view) {
        relativeFilter.bringToFront();
        relativeFilter.setVisibility(View.VISIBLE);
        relativeMenu.setVisibility(View.GONE);
        RelativeLayout fashionBox = findViewById(R.id.fashion_section);
        RelativeLayout ratingsBox = findViewById(R.id.ratings_box);
        RelativeLayout categoryBox = findViewById(R.id.category_section);
        ViewGroup.LayoutParams params = relativeDrawer.getLayoutParams();
        float density = this.getResources().getDisplayMetrics().density;
        float px = 250 * density;
        params.width = (int) px;
        relativeDrawer.setLayoutParams(params);
        if (control == BottomNav.PRODUCTS) {
            fashionBox.setVisibility(View.GONE);
            ratingsBox.setVisibility(View.GONE);
            categoryBox.setVisibility(View.VISIBLE);
            categoryBox.bringToFront();
        } else {
            fashionBox.setVisibility(View.VISIBLE);
            fashionBox.bringToFront();
            ratingsBox.setVisibility(View.VISIBLE);
            categoryBox.setVisibility(View.GONE);
        }
        openDrawer(drawerLayout);
    }

    public void ApplyClick(View view) throws JSONException {
        setFilter();
        editMin.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideKeyboard(v);
        });

        editMax.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideKeyboard(v);
        });

        closeDrawer(drawerLayout);
    }

    public void OneRatingClick(View view) {
        currentRating = 1;
    }

    public void TwoRatingClick(View view) {
        currentRating = 2;
    }

    public void ThreeRatingClick(View view) {
        currentRating = 3;
    }

    public void FourRatingClick(View view) {
        currentRating = 4;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setFilter() throws JSONException {
        String minPrice = editMin.getText().toString().trim();
        String maxPrice = editMax.getText().toString().trim();
        try {
            filterJSON.put("fashionFilter", "");
            filterJSON.put("brandFilter", "");
            if (minPrice.isEmpty())
                filterJSON.put("min", "");
            else
                filterJSON.put("min", minPrice);
            if (maxPrice.isEmpty())
                filterJSON.put("max", "");
            else
                filterJSON.put("max", maxPrice);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String brandFilter = "", fashionFilter = "", categoryFilter = "";

        if (!myWardrobeCheck.isChecked()) {
            if (mangoCheck.isChecked())
                brandFilter += Brands.MANGO.toString();
            if (hmCheck.isChecked())
                brandFilter += Brands.HM.toString();
            if (twistCheck.isChecked())
                brandFilter += Brands.TWIST.toString();
            if (bershkaCheck.isChecked())
                brandFilter += Brands.BERSHKA.toString();
            if (ipekyolCheck.isChecked())
                brandFilter += Brands.IPEKYOL.toString();
        }

        if (businessCheck.isChecked())
            fashionFilter += FashionType.BUSINESS.toString();
        if (weekendCheck.isChecked())
            fashionFilter += FashionType.WEEKEND.toString();
        if (dinnerCheck.isChecked())
            fashionFilter += FashionType.DINNER.toString();
        if (summerCheck.isChecked())
            fashionFilter += FashionType.SUMMER.toString();
        if (winterCheck.isChecked())
            fashionFilter += FashionType.WINTER.toString();
        if (fallCheck.isChecked())
            fashionFilter += FashionType.FALL.toString();
        if (casualCheck.isChecked())
            fashionFilter += FashionType.CASUAL.toString();

        if (hatCheck.isChecked())
            categoryFilter += Type.SAPKA.toString();
        if (dressCheck.isChecked())
            categoryFilter += Type.ELBISE.toString();
        if (upperCheck.isChecked())
            categoryFilter += Type.UST.toString();
        if (bottomCheck.isChecked())
            categoryFilter += Type.ALT.toString();
        if (glassesCheck.isChecked())
            categoryFilter += Type.GOZLUK.toString();
        if (shoesCheck.isChecked())
            categoryFilter += Type.AYAKKABI.toString();
        if (bagCheck.isChecked())
            categoryFilter += Type.CANTA.toString();

        try {
            filterJSON.put("brandFilter", brandFilter);
            filterJSON.put("fashionFilter", fashionFilter);
            filterJSON.put("categoryFilter", categoryFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (control == BottomNav.HOME || control == BottomNav.MY) {
            updateCombines();
            updateMyCombines();
            categorize();
        } else
            categorizeProduct();
    }

    public void HomeClick(View view) {
        Log.d(TAG, "onClick: Home");
    }

    public void MyCombineClick(View view) {
        Log.d(TAG, "onClick: myCombines");
        if (mAuth.getCurrentUser() != null) {
            redirectActivity(MainActivity.this, MyCombines.class);
            // finish();
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void MyWardrobeClick(View view) {
        Log.d(TAG, "onClick: myWardrobe");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(MainActivity.this, MyWardrobe.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void CreateCombineClick(View view) {
        Log.d(TAG, "onClick: createCombine");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(MainActivity.this, CreateCombine.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void LogOutClick(View view) {
        Log.d(TAG, "onClick: logOut");
        user = null;
        mAuth.signOut();
        finish();
        redirectActivity(MainActivity.this, MainActivity.class);
    }

    public void LogInClick(View view) {
        Log.d(TAG, "onClick: logIn");
        finish();
        redirectActivity(MainActivity.this, Login.class);
    }

    public void SignUpClick(View view) {
        Log.d(TAG, "onClick: signUp");
        finish();
        redirectActivity(MainActivity.this, Register.class);
    }

    private static void redirectActivity(Activity activity, Class<?> aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void initializeById() {
        textUser = findViewById(R.id.text_user);
        drawerLayout = findViewById(R.id.drawer_layout);
        relativeFilter = findViewById(R.id.relative_filter);
        relativeMenu = findViewById(R.id.relative_menu);
        linearLogin = findViewById(R.id.linear_login);
        linearLogout = findViewById(R.id.linear_logout);
        frameLayout = findViewById(R.id.fragment_container);
        bottomNav = findViewById(R.id.bottom_navigation);
        mangoCheck = findViewById(R.id.check_brand1);
        hmCheck = findViewById(R.id.check_brand2);
        twistCheck = findViewById(R.id.check_brand3);
        bershkaCheck = findViewById(R.id.check_brand4);
        ipekyolCheck = findViewById(R.id.check_brand5);
        businessCheck = findViewById(R.id.check_fashion1);
        weekendCheck = findViewById(R.id.check_fashion2);
        dinnerCheck = findViewById(R.id.check_fashion3);
        summerCheck = findViewById(R.id.check_fashion4);
        winterCheck = findViewById(R.id.check_fashion5);
        fallCheck = findViewById(R.id.check_fashion6);
        casualCheck = findViewById(R.id.check_fashion7);
        hatCheck = findViewById(R.id.check_category1);
        glassesCheck = findViewById(R.id.check_category2);
        dressCheck = findViewById(R.id.check_category3);
        upperCheck = findViewById(R.id.check_category4);
        bottomCheck = findViewById(R.id.check_category5);
        bagCheck = findViewById(R.id.check_category6);
        shoesCheck = findViewById(R.id.check_category7);
        myWardrobeCheck = findViewById(R.id.check_wardrobe);
        editMin = findViewById(R.id.edit_min);
        editMax = findViewById(R.id.edit_max);
        relativeDrawer = findViewById(R.id.relative_drawer);
        TextView tittle = findViewById(R.id.text_title);
        tittle.setText(TITLE);
    }

    private void categorize() throws JSONException {
        ArrayList<Combine> currentList = new ArrayList<>();
        ArrayList<Combine> tempList;
        if (control == BottomNav.HOME) {
            if (combines.size() == 0)
                setHomeList();
            tempList = new ArrayList<>(combines);
        } else {
            if (myCombines.size() == 0)
                setUserCombineList();
            tempList = new ArrayList<>(myCombines);
        }
        String filterBrands = (String) filterJSON.get("brandFilter");
        String filterFashion = (String) filterJSON.get("fashionFilter");
        String filterMinPrice = (String) filterJSON.get("min");
        String filterMaxPrice = (String) filterJSON.get("max");

        for (Combine combine : tempList) {
            ArrayList<FashionType> fashionTypes = combine.getFashionTypes();
            boolean controlBrand;
            boolean controlFashion = false;

            if (filterFashion.equals(""))
                controlFashion = true;
            else {
                if (fashionTypes != null) {
                    for (FashionType fashionType : fashionTypes) {
                        if (filterFashion.contains(fashionType.toString())) {
                            controlFashion = true;
                            break;
                        }
                    }
                }
            }

            if (myWardrobeCheck.isChecked()) {
                boolean isInMyWardrobe;
                for (Product p : myWardrobe) {
                    if (!p.isVisible()) {
                        isInMyWardrobe = false;
                    } else if (combine.getBag() != null && p.getId().equals(combine.getBag().getId())) {
                        isInMyWardrobe = true;
                    } else if (combine.getBottomGarment() != null && p.getId().equals(combine.getBottomGarment().getId())) {
                        isInMyWardrobe = true;
                    } else if (combine.getDress() != null && p.getId().equals(combine.getDress().getId())) {
                        isInMyWardrobe = true;
                    } else if (combine.getGlasses() != null && p.getId().equals(combine.getGlasses().getId())) {
                        isInMyWardrobe = true;
                    } else if (combine.getShoes() != null && p.getId().equals(combine.getShoes().getId())) {
                        isInMyWardrobe = true;
                    } else if (combine.getUpperGarment() != null && p.getId().equals(combine.getUpperGarment().getId())) {
                        isInMyWardrobe = true;
                    } else {
                        isInMyWardrobe = combine.getHat() != null && p.getId().equals(combine.getHat().getId());
                    }

                    if (isInMyWardrobe && controlFashion
                            && (filterMinPrice.isEmpty() || Double.parseDouble(filterMinPrice) < combine.getPrice())
                            && (filterMaxPrice.isEmpty() || Double.parseDouble(filterMaxPrice) > combine.getPrice())
                            && (currentRating <= combine.getRate())) {
                        currentList.add(combine);
                        break;
                    }
                }
            } else {
                if (filterBrands.equals(""))
                    controlBrand = true;
                else {
                    if (combine.getBag() != null && filterBrands.contains(combine.getBag().getBrand()))
                        controlBrand = true;
                    else if (combine.getBottomGarment() != null && filterBrands.contains(combine.getBottomGarment().getBrand()))
                        controlBrand = true;
                    else if (combine.getDress() != null && filterBrands.contains(combine.getDress().getBrand()))
                        controlBrand = true;
                    else if (combine.getGlasses() != null && filterBrands.contains(combine.getGlasses().getBrand()))
                        controlBrand = true;
                    else if (combine.getShoes() != null && filterBrands.contains(combine.getShoes().getBrand()))
                        controlBrand = true;
                    else if (combine.getUpperGarment() != null && filterBrands.contains(combine.getUpperGarment().getBrand()))
                        controlBrand = true;
                    else
                        controlBrand = combine.getHat() != null && filterBrands.contains(combine.getHat().getBrand());
                }
                if (controlBrand && controlFashion
                        && (filterMinPrice.isEmpty() || Double.parseDouble(filterMinPrice) < combine.getPrice())
                        && (filterMaxPrice.isEmpty() || Double.parseDouble(filterMaxPrice) > combine.getPrice())
                        && (currentRating <= combine.getRate()))
                    currentList.add(combine);
            }
        }
        if (control == BottomNav.HOME) {
            selectedFragment = new HomeFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("temp", currentList);
            bundle.putBoolean("control", false);
            selectedFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(frameLayout.getId(), selectedFragment).commit();
        } else if (control == BottomNav.MY) {
            selectedFragment = new MyCombinesFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("temp", currentList);
            bundle.putBoolean("control", false);
            selectedFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(frameLayout.getId(), selectedFragment).commit();
        }
    }

    private void getProductStartScreen() {
        relativeFilter.bringToFront();
        relativeFilter.setVisibility(View.VISIBLE);
        relativeMenu.setVisibility(View.GONE);
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        int screenWidth = size.x;
        ViewGroup.LayoutParams params = relativeDrawer.getLayoutParams();
        RelativeLayout fashionBox = findViewById(R.id.fashion_section);
        RelativeLayout categoryBox = findViewById(R.id.category_section);
        RelativeLayout ratingsBox = findViewById(R.id.ratings_box);
        params.width = screenWidth;
        relativeDrawer.setLayoutParams(params);
        openDrawer(drawerLayout);
        fashionBox.setVisibility(View.GONE);
        ratingsBox.setVisibility(View.GONE);
        categoryBox.setVisibility(View.VISIBLE);
        categoryBox.bringToFront();
    }

    private void categorizeProduct() throws JSONException {
        ArrayList<Product> currentList = new ArrayList<>();
        String filterBrands = (String) filterJSON.get("brandFilter");
        String filterMinPrice = (String) filterJSON.get("min");
        String filterMaxPrice = (String) filterJSON.get("max");
        String filterCategory = (String) filterJSON.get("categoryFilter");
        for (Product product : products) {
            String price = product.getPrice().replace(".", "");
            price = price.replace(',', '.');
            price = price.replace('₺', ' ');
            price = price.replace("TL", " ");
            if (myWardrobeCheck.isChecked()) {
                for (Product p : myWardrobe) {
                    if (p.getId().equals(product.getId()) && p.isVisible())
                        currentList.add(product);
                }
            } else {
                if ((filterBrands.equals("") || filterBrands.contains(product.getBrand()))
                        && (filterMinPrice.isEmpty() || Double.parseDouble(filterMinPrice) < Double.parseDouble(price))
                        && (filterMaxPrice.isEmpty() || Double.parseDouble(filterMaxPrice) > Double.parseDouble(price))
                        && (filterCategory.equals("") || filterCategory.contains(product.getFilterType().toString())))
                    currentList.add(product);
            }
        }
        selectedFragment = new ProductsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("temp", currentList);
        bundle.putString("screenTag", "HomeProducts");
        selectedFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(frameLayout.getId(), selectedFragment).commit();
    }

    private void setHomeList() {
        mDatabaseReference.child(COMBINES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        final Combine readCombine = ds.getValue(Combine.class);
                        assert readCombine != null;
                        readCombine.setId(ds.getKey());
                        if (readCombine.isVisible() && !combines.contains(readCombine))
                            combines.add(readCombine);
                    }
                }
                selectedFragment = new HomeFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("temp", combines);
                bundle.putBoolean("control", true);
                selectedFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(frameLayout.getId(), selectedFragment).commit();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setUserCombineList() {
        String userId = mAuth.getUid();
        assert userId != null;
        mDatabaseReference.child(USERS).child(userId).child(myCOMBINES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        final Combine readCombine = ds.getValue(Combine.class);
                        assert readCombine != null;
                        if (!myCombines.contains(readCombine))
                            myCombines.add(readCombine);
                    }
                }
                selectedFragment = new MyCombinesFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("temp", myCombines);
                bundle.putBoolean("control", true);
                selectedFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(frameLayout.getId(), selectedFragment).commit();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setProductList() {
        if (products.size() == 0) {
            mDatabaseReference.child(PRODUCTS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        for (DataSnapshot ds : snapshot.getChildren()) {
                            final Product readProduct = ds.getValue(Product.class);

                            assert readProduct != null;
                            products.add(readProduct);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private void setWardrobe() {
        String userId = mAuth.getUid();
        assert userId != null;
        mDatabaseReference.child(USERS).child(userId).child(myWARDROBE).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        final Product readProduct = ds.getValue(Product.class);
                        assert readProduct != null;
                        boolean check = false;
                        int index = 0;
                        for (; index < myWardrobe.size(); index++) {
                            if (myWardrobe.get(index).getId().equals(readProduct.getId())) {
                                check = true;
                                break;
                            }
                        }

                        if (!check)
                            index = -1;

                        if (readProduct.isVisible() && !check) {
                            myWardrobe.add(readProduct);
                        }
                        if (index != -1 && check && !readProduct.isVisible())
                            myWardrobe.remove(index);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onCombineClick(int position) {
        if (combines != null) {
            Log.d(TAG, "onClick: combineDetails");
            finish();
            Combine selectedCombine = combines.get(position);
            Intent myIntent = new Intent(MainActivity.this, CombineDetails.class);
            myIntent.putExtra("Combine", selectedCombine);
            startActivity(myIntent);
        }
    }

    public static User getCurrentUser() {
        return user;
    }

    private void resetFilter() {
        editMin.setText("");
        editMax.setText("");
        currentRating = 0;
        mangoCheck.setChecked(false);
        hmCheck.setChecked(false);
        twistCheck.setChecked(false);
        bershkaCheck.setChecked(false);
        ipekyolCheck.setChecked(false);
        businessCheck.setChecked(false);
        weekendCheck.setChecked(false);
        dinnerCheck.setChecked(false);
        summerCheck.setChecked(false);
        winterCheck.setChecked(false);
        fallCheck.setChecked(false);
        casualCheck.setChecked(false);
        hatCheck.setChecked(false);
        glassesCheck.setChecked(false);
        dressCheck.setChecked(false);
        upperCheck.setChecked(false);
        bottomCheck.setChecked(false);
        bagCheck.setChecked(false);
        shoesCheck.setChecked(false);
        myWardrobeCheck.setChecked(false);
        try {
            filterJSON.put("brandFilter", "");
            filterJSON.put("min", "");
            filterJSON.put("max", "");
            filterJSON.put("fashionFilter", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateCombines() {
        mDatabaseReference.child(COMBINES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        final Combine readCombine = ds.getValue(Combine.class);
                        assert readCombine != null;
                        boolean check = false;
                        int index = 0;
                        for (; index < combines.size(); index++) {
                            if (combines.get(index).getId().equals(readCombine.getId())) {
                                check = true;
                                break;
                            }
                        }

                        if (!check)
                            index = -1;

                        if (readCombine.isVisible() && !check) {
                            combines.add(readCombine);
                            MergeSort ms = new MergeSort(combines);
                            ms.sortGivenArray();
                            combines = ms.getSortedArray();
                        }
                        if (index != -1 && check && !readCombine.isVisible())
                            combines.remove(index);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void updateMyCombines() {
        mDatabaseReference.child(COMBINES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        final Combine readCombine = ds.getValue(Combine.class);
                        assert readCombine != null;
                        readCombine.setId(ds.getKey());
                        for (Combine combine : myCombines) {
                            if (combine.getId().equals(readCombine.getId())) {
                                combine.setVisible(readCombine.isVisible());
                                break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public static class MergeSort {
        private final ArrayList<Combine> inputArray;

        public ArrayList<Combine> getSortedArray() {
            return inputArray;
        }

        public MergeSort(ArrayList<Combine> inputArray){
            this.inputArray = inputArray;
        }

        public void sortGivenArray(){
            divide(0, this.inputArray.size() - 1);
        }

        public void divide(int startIndex,int endIndex){

            //Divide till you breakdown your list to single element
            if(startIndex < endIndex && (endIndex - startIndex) >= 1){
                int mid = (endIndex + startIndex) / 2;
                divide(startIndex, mid);
                divide(mid + 1, endIndex);

                //merging Sorted array produce above into one sorted array
                merger(startIndex,mid,endIndex);
            }
        }

        public void merger(int startIndex,int midIndex,int endIndex){

            //Below is the mergedarray that will be sorted array Array[i-midIndex] , Array[(midIndex+1)-endIndex]
            ArrayList<Combine> mergedSortedArray = new ArrayList<>();

            int leftIndex = startIndex;
            int rightIndex = midIndex+1;

            while(leftIndex<=midIndex && rightIndex<=endIndex){
                float leftNum = Float.parseFloat(inputArray.get(leftIndex).getId());
                float rightNum = Float.parseFloat(inputArray.get(rightIndex).getId());
                if(leftNum <= rightNum){
                    mergedSortedArray.add(inputArray.get(leftIndex));
                    leftIndex++;
                }else{
                    mergedSortedArray.add(inputArray.get(rightIndex));
                    rightIndex++;
                }
            }

            //Either of below while loop will execute
            while(leftIndex <= midIndex){
                mergedSortedArray.add(inputArray.get(leftIndex));
                leftIndex++;
            }

            while(rightIndex <= endIndex){
                mergedSortedArray.add(inputArray.get(rightIndex));
                rightIndex++;
            }

            int i = 0;
            int j = startIndex;
            //Setting sorted array to original one
            while(i<mergedSortedArray.size()){
                inputArray.set(j, mergedSortedArray.get(i++));
                j++;
            }
        }
    }
}