package com.example.clothescombiner.Activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.R;
import com.squareup.picasso.Picasso;

import static com.example.clothescombiner.Activities.MainActivity.user;
import static com.example.clothescombiner.Activities.SplashScreen.mAuth;
import static com.example.clothescombiner.Activities.SplashScreen.mDatabaseReference;

public class ProductDetail extends Activity {
    private static final String TITLE = "Product Detail";
    private static final String TAG = "ProductDetail";
    private static final String USERS = "Users";
    private static final String myWARDROBE = "MyWardrobe";
    private TextView textUser;
    private ImageView productImage;
    private TextView productName;
    private TextView productBrand;
    private TextView productColor;
    private TextView productGender;
    private TextView productPrice;
    private RelativeLayout relativeMenu;
    private RelativeLayout relativeFilter;
    private LinearLayout linearLogout;
    private LinearLayout linearLogin;
    private DrawerLayout drawerLayout;

    private Product activeProduct;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        Intent i = getIntent();
        activeProduct = (Product) i.getSerializableExtra("Product");

        initializeById();

        Picasso.get()
                .load(activeProduct.getImage())
                .into(productImage);
        productPrice.setText(activeProduct.getPrice());
        productGender.setText(activeProduct.getGender());
        productColor.setText(activeProduct.getColor());
        productBrand.setText(activeProduct.getBrand());
        productName.setText(activeProduct.getName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (user != null) {
            linearLogin.setVisibility(View.VISIBLE);
            linearLogout.setVisibility(View.INVISIBLE);
            textUser.setText(user.getFullName());
        }

        if (user == null) {
            linearLogin.setVisibility(View.INVISIBLE);
            linearLogout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

    public void ClickMenu(View view) {
        relativeMenu.bringToFront();
        relativeMenu.setVisibility(View.VISIBLE);
        relativeFilter.setVisibility(View.GONE);
        openDrawer(drawerLayout);
    }

    public void WebPageClick(View view) {
        Log.d(TAG, "onClick: webPage");
        Intent browse = new Intent(Intent.ACTION_VIEW, Uri.parse(activeProduct.getUrl()));
        startActivity(browse);
    }

    public void HomeClick(View view) {
        Log.d(TAG, "onClick: Home");
        redirectActivity(ProductDetail.this, MainActivity.class);
        finish();
    }

    public void MyCombineClick(View view) {
        Log.d(TAG, "onClick: myCombines");
        if (mAuth.getCurrentUser() != null) {
            redirectActivity(ProductDetail.this, MyCombines.class);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void AddWardrobe(View view) {
        Log.d(TAG, "onClick: addWardrobe");
        if (mAuth.getCurrentUser() != null) {
            String userId = mAuth.getUid();
            activeProduct.setVisible(true);
            assert userId != null;
            mDatabaseReference.child(USERS).child(userId).child(myWARDROBE)
                    .child(String.valueOf(activeProduct.getId())).setValue(activeProduct);
            Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void CreateCombineClick(View view) {
        Log.d(TAG, "onClick: createCombine");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(ProductDetail.this, CreateCombine.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void MyWardrobeClick(View view) {
        Log.d(TAG, "onClick: myWardrobe");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(ProductDetail.this, MyWardrobe.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void LogOutClick(View view) {
        Log.d(TAG, "onClick: logOut");
        user = null;
        mAuth.signOut();
        finish();
        Intent myIntent = new Intent(ProductDetail.this, ProductDetail.class);
        myIntent.putExtra("Product", activeProduct);
        startActivity(myIntent);
    }

    public void LogInClick(View view) {
        Log.d(TAG, "onClick: logIn");
        finish();
        redirectActivity(ProductDetail.this, Login.class);
    }

    public void SignUpClick(View view) {
        Log.d(TAG, "onClick: signUp");
        finish();
        redirectActivity(ProductDetail.this, Register.class);
    }

    private static void redirectActivity(Activity activity, Class<?> aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void initializeById() {
        textUser = findViewById(R.id.text_user);
        linearLogin = findViewById(R.id.linear_login);
        linearLogout = findViewById(R.id.linear_logout);
        drawerLayout = findViewById(R.id.drawer_layout);
        relativeMenu = findViewById(R.id.relative_menu);
        relativeFilter = findViewById(R.id.relative_filter);
        productImage = findViewById(R.id.image_product);
        productName = findViewById(R.id.text_namevalue);
        productBrand = findViewById(R.id.text_brandvalue);
        productColor = findViewById(R.id.text_colorvalue);
        productGender = findViewById(R.id.text_gendervalue);
        productPrice = findViewById(R.id.text_pricevalue);
        ImageView buttonFilter = findViewById(R.id.button_filter);
        buttonFilter.setVisibility(View.GONE);
        TextView tittle = findViewById(R.id.text_title);
        tittle.setText(TITLE);
        if (user != null)
            textUser.setText(user.getFullName());
    }
}
