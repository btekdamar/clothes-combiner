package com.example.clothescombiner.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.clothescombiner.R;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

import static com.example.clothescombiner.Activities.SplashScreen.mAuth;

public class Login extends Activity {
    private final static String TAG = "Login";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        final EditText username = findViewById(R.id.edit_username);
        final EditText password = findViewById(R.id.edit_password);
        TextView forgotPassword = findViewById(R.id.text_forgot);
        TextView signUp = findViewById(R.id.text_signup);
        Button signIn = findViewById(R.id.button_sign);

        if (mAuth.getCurrentUser() != null)
            mAuth.signOut();

        signIn.setOnClickListener(v -> {
            String name = username.getText().toString().trim();
            String pass = password.getText().toString().trim();

            if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(pass)) {
                mAuth.signInWithEmailAndPassword(name, pass).addOnCompleteListener(task -> {
                    FirebaseUser user = mAuth.getCurrentUser();
                    if (user == null) {
                        Log.d(TAG, "login: failure");
                        toastMessage(Objects.requireNonNull(task.getException()).getMessage());
                    } else if (task.isSuccessful() && user.isEmailVerified()) {
                        finish();
                        Log.d(TAG, "login: success");
                        Intent myIntent = new Intent(Login.this, MainActivity.class);
                        startActivity(myIntent);
                    } else if (!user.isEmailVerified()) {
                        Log.d(TAG, "emailVerified: failure");
                        toastMessage("Please verify email");
                        mAuth.signOut();
                    } else {
                        Log.d(TAG, "login: failure");
                        toastMessage(Objects.requireNonNull(task.getException()).getMessage());
                    }
                });
            }
        });

        signUp.setOnClickListener(v -> {
            Login.this.finish();
            Intent myIntent = new Intent(Login.this, Register.class);
            startActivityForResult(myIntent, 0);
        });

        forgotPassword.setOnClickListener(v -> {
            Log.d(TAG, "onClick: forgotPassword");
            Intent forgotIntent = new Intent(Login.this, ForgotPassword.class);
            startActivity(forgotIntent);
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    private void toastMessage(String message) {
        Toast.makeText(Login.this, message, Toast.LENGTH_SHORT).show();
    }
}
