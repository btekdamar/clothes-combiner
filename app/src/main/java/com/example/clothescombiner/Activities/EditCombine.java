package com.example.clothescombiner.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.clothescombiner.Adapters.CreateProductsAdapter;
import com.example.clothescombiner.Core.Brands;
import com.example.clothescombiner.Core.Combine;
import com.example.clothescombiner.Core.FashionType;
import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.Core.Type;
import com.example.clothescombiner.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import static com.example.clothescombiner.Activities.MainActivity.myWardrobe;
import static com.example.clothescombiner.Activities.MainActivity.products;
import static com.example.clothescombiner.Activities.SplashScreen.mAuth;
import static com.example.clothescombiner.Activities.SplashScreen.mDatabaseReference;

public class EditCombine extends Activity implements CreateProductsAdapter.OnProductListener {
    private static final String TAG = "EditCombine";
    private static final String TITLE = "Edit Combine";
    private static final String USERS = "Users";
    private static final String COMBINES = "Combines";
    private static final String myCOMBINES = "MyCombines";
    private boolean isOpen = false;
    private Type activeType = null;
    private ToggleButton business, weekend, dinner, summer, winter, fall, casual;

    private ImageView hat, glasses, bag, shoes, dress, upper, bottom;
    private RecyclerView recyclerView;
    private ArrayList<Product> currentList;
    private Animation mFabOpenAnim, mFabCloseAnim;
    private FloatingActionButton fabShoes;
    private FloatingActionButton fabShirt;
    private FloatingActionButton fabPants;
    private FloatingActionButton fabHat;
    private FloatingActionButton fabBag;
    private FloatingActionButton fabGlasses;
    private FloatingActionButton fabDress;
    private RelativeLayout relativeFilter;
    private RelativeLayout relativeMenu;
    private DrawerLayout drawerLayout;

    private CheckBox mangoCheck, hmCheck, twistCheck, bershkaCheck, ipekyolCheck, myWardrobeCheck;
    private EditText editMin, editMax;
    private final JSONObject filterJSON = new JSONObject();
    private Combine activeCombine;
    private Combine combine;

    private ArrayList<FashionType> fashionArray = new ArrayList<>();

    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_combine);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        Intent i = getIntent();
        activeCombine = (Combine) i.getSerializableExtra("Combine");
        assert activeCombine != null;
        combine = new Combine(activeCombine);
        fashionArray = combine.getFashionTypes();
        initializeById();
        setImage();

        FloatingActionButton fabMain = findViewById(R.id.fab_main);
        fabMain.setOnClickListener(v -> {
            mFabOpenAnim = AnimationUtils.loadAnimation(EditCombine.this, R.anim.fab_open);
            mFabCloseAnim = AnimationUtils.loadAnimation(EditCombine.this, R.anim.fab_close);
            fabOpenClose();
        });

        fabBag.setOnClickListener(v -> {
            Log.d(TAG, "Bag clicked");
            activeType = Type.CANTA;
            setFilter();
            try {
                categorize();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fabOpenClose();
        });

        fabShoes.setOnClickListener(v -> {
            Log.d(TAG, "Shoes clicked");
            activeType = Type.AYAKKABI;
            setFilter();
            try {
                categorize();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fabOpenClose();
        });

        fabShirt.setOnClickListener(v -> {
            Log.d(TAG, "Upper clicked");
            activeType = Type.UST;
            setFilter();
            try {
                categorize();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fabOpenClose();
        });

        fabPants.setOnClickListener(v -> {
            Log.d(TAG, "Bottom clicked");
            activeType = Type.ALT;
            setFilter();
            try {
                categorize();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fabOpenClose();
        });

        fabHat.setOnClickListener(v -> {
            Log.d(TAG, "Hat clicked");
            activeType = Type.SAPKA;
            setFilter();
            try {
                categorize();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fabOpenClose();
        });

        fabDress.setOnClickListener(v -> {
            Log.d(TAG, "Dress clicked");
            activeType = Type.ELBISE;
            setFilter();
            try {
                categorize();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fabOpenClose();
        });

        fabGlasses.setOnClickListener(v -> {
            Log.d(TAG, "Glasses clicked");
            activeType = Type.GOZLUK;
            setFilter();
            try {
                categorize();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            fabOpenClose();
        });

        myWardrobeCheck.setOnClickListener(v -> {
            if (myWardrobeCheck.isChecked()) {
                mangoCheck.setClickable(false);
                hmCheck.setClickable(false);
                twistCheck.setClickable(false);
                bershkaCheck.setClickable(false);
                ipekyolCheck.setClickable(false);
            } else {
                mangoCheck.setClickable(true);
                hmCheck.setClickable(true);
                twistCheck.setClickable(true);
                bershkaCheck.setClickable(true);
                ipekyolCheck.setClickable(true);
            }
        });


        initList(products);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    public void ClickMenu(View view) {
        relativeMenu.bringToFront();
        relativeMenu.setVisibility(View.VISIBLE);
        relativeFilter.setVisibility(View.GONE);
        openDrawer(drawerLayout);
    }

    public void FilterClick(View view) {
        RelativeLayout fashionBox = findViewById(R.id.relative_fashion_category);
        fashionBox.setVisibility(View.GONE);
        RelativeLayout ratingsBox = findViewById(R.id.ratings_box);
        ratingsBox.setVisibility(View.GONE);
        Button applyButton = findViewById(R.id.button_apply);
        applyButton.setVisibility(View.VISIBLE);
        applyButton.bringToFront();
        relativeFilter.bringToFront();
        relativeFilter.setVisibility(View.VISIBLE);
        relativeMenu.setVisibility(View.GONE);
        openDrawer(drawerLayout);
    }

    public void ApplyClick(View view) throws JSONException {
        setFilter();
        categorize();
        closeDrawer(drawerLayout);
        editMin.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideKeyboard(v);
        });

        editMax.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideKeyboard(v);
        });
    }

    public void SaveClick(View view) {
        if (!combine.equals(activeCombine)) {
            Log.d(TAG, "onClick: save");
            final Dialog myDialog = new Dialog(EditCombine.this);
            myDialog.setContentView(R.layout.custom_save_dialog);
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            myDialog.setTitle("Combine Name");
            business = myDialog.findViewById(R.id.toggle_business);
            weekend = myDialog.findViewById(R.id.toggle_weekend);
            dinner = myDialog.findViewById(R.id.toggle_dinner);
            summer = myDialog.findViewById(R.id.toggle_summer);
            winter = myDialog.findViewById(R.id.toggle_winter);
            fall = myDialog.findViewById(R.id.toggle_fall);
            casual = myDialog.findViewById(R.id.toggle_casual);

            for (FashionType fashionType : fashionArray) {
                if (fashionType == FashionType.BUSINESS)
                    business.setChecked(true);
                if (fashionType == FashionType.WEEKEND)
                    weekend.setChecked(true);
                if (fashionType == FashionType.DINNER)
                    dinner.setChecked(true);
                if (fashionType == FashionType.SUMMER)
                    summer.setChecked(true);
                if (fashionType == FashionType.WINTER)
                    winter.setChecked(true);
                if (fashionType == FashionType.FALL)
                    fall.setChecked(true);
                if (fashionType == FashionType.CASUAL)
                    casual.setChecked(true);
            }

            TextView save = myDialog.findViewById(R.id.text_save);
            save.setOnClickListener(v -> {
                @SuppressLint("CutPasteId")
                final EditText input = myDialog.findViewById(R.id.edit_name);
                if (input != null) {
                    input.setOnFocusChangeListener((v1, hasFocus) -> {
                        if (!hasFocus)
                            hideKeyboard(v1);
                    });
                }
                String combineName = input.getText().toString();
                if (!TextUtils.isEmpty(combineName) && combineName.length() < 17) {
                    combine.setName(combineName);
                    combine.setUser(MainActivity.user.getFullName());
                    combine.calculatePrice();
                    combine.setFashionTypes(fashionArray);
                    updateCombineDatabase();
                    myDialog.dismiss();
                    finish();
                    redirectActivity(EditCombine.this, MainActivity.class);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Combine Name", Toast.LENGTH_SHORT).show();
                }
            });
            myDialog.show();
        } else {
            Toast.makeText(getApplicationContext(),
                    "You have not made any changes to the combine!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void CancelClick(View view) {
        Log.d(TAG, "onClick: cancel");
        finish();
        redirectActivity(EditCombine.this, MainActivity.class);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setFilter() {
        String minPrice = editMin.getText().toString().trim();
        String maxPrice = editMax.getText().toString().trim();
        try {
            filterJSON.put("brandFilter", "");
            if (minPrice.isEmpty())
                filterJSON.put("min", "");
            else
                filterJSON.put("min", minPrice);
            if (maxPrice.isEmpty())
                filterJSON.put("max", "");
            else
                filterJSON.put("max", maxPrice);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String brandFilter = "";

        if (mangoCheck.isChecked())
            brandFilter += Brands.MANGO.toString();
        if (hmCheck.isChecked())
            brandFilter += Brands.HM.toString();
        if (twistCheck.isChecked())
            brandFilter += Brands.TWIST.toString();
        if (bershkaCheck.isChecked())
            brandFilter += Brands.BERSHKA.toString();
        if (ipekyolCheck.isChecked())
            brandFilter += Brands.IPEKYOL.toString();

        try {
            filterJSON.put("brandFilter", brandFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void HomeClick(View view) {
        Log.d(TAG, "onClick: Home");
        if (mAuth.getCurrentUser() != null) {
            redirectActivity(EditCombine.this, MainActivity.class);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void MyCombineClick(View view) {
        Log.d(TAG, "onClick: myCombines");
        if (mAuth.getCurrentUser() != null) {
            redirectActivity(EditCombine.this, MyCombines.class);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void MyWardrobeClick(View view) {
        Log.d(TAG, "onClick: myWardrobe");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(EditCombine.this, MyWardrobe.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void CreateCombineClick(View view) {
        Log.d(TAG, "onClick: createCombine");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(EditCombine.this, CreateCombine.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void BusinessClick(View view) {
        if (business.isChecked())
            fashionArray.add(FashionType.BUSINESS);
        else
            fashionArray.remove(FashionType.BUSINESS);
    }

    public void WeekendClick(View view) {
        if (weekend.isChecked())
            fashionArray.add(FashionType.WEEKEND);
        else
            fashionArray.remove(FashionType.WEEKEND);
    }

    public void DinnerClick(View view) {
        if (dinner.isChecked())
            fashionArray.add(FashionType.DINNER);
        else
            fashionArray.remove(FashionType.DINNER);

    }

    public void SummerClick(View view) {
        if (summer.isChecked())
            fashionArray.add(FashionType.SUMMER);
        else
            fashionArray.remove(FashionType.SUMMER);
    }

    public void WinterClick(View view) {
        if (winter.isChecked())
            fashionArray.add(FashionType.WINTER);
        else
            fashionArray.remove(FashionType.WINTER);
    }

    public void FallClick(View view) {
        if (fall.isChecked())
            fashionArray.add(FashionType.FALL);
        else
            fashionArray.remove(FashionType.FALL);
    }

    public void CasualClick(View view) {
        if (casual.isChecked())
            fashionArray.add(FashionType.CASUAL);
        else
            fashionArray.remove(FashionType.CASUAL);
    }

    public void LogOutClick(View view) {
        Log.d(TAG, "onClick: logOut");
        MainActivity.user = null;
        mAuth.signOut();
        finish();
        redirectActivity(EditCombine.this, MainActivity.class);
    }

    private static void redirectActivity(Activity activity, Class<?> aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void initializeById() {
        recyclerView = findViewById(R.id.list_products);
        hat = findViewById(R.id.image_hat);
        bag = findViewById(R.id.image_bag);
        glasses = findViewById(R.id.image_glasses);
        shoes = findViewById(R.id.image_shoes);
        upper = findViewById(R.id.image_upperGarment);
        bottom = findViewById(R.id.image_bottomGarment);
        dress = findViewById(R.id.image_dress);
        fabBag = findViewById(R.id.fab_bag);
        fabDress = findViewById(R.id.fab_dress);
        fabGlasses = findViewById(R.id.fab_glasses);
        fabHat = findViewById(R.id.fab_hat);
        fabPants = findViewById(R.id.fab_bottom);
        fabShirt = findViewById(R.id.fab_upper);
        fabShoes = findViewById(R.id.fab_shoes);
        drawerLayout = findViewById(R.id.drawer_layout);
        relativeFilter = findViewById(R.id.relative_filter);
        relativeMenu = findViewById(R.id.relative_menu);
        editMin = findViewById(R.id.edit_min);
        editMax = findViewById(R.id.edit_max);
        mangoCheck = findViewById(R.id.check_brand1);
        hmCheck = findViewById(R.id.check_brand2);
        twistCheck = findViewById(R.id.check_brand3);
        bershkaCheck = findViewById(R.id.check_brand4);
        ipekyolCheck = findViewById(R.id.check_brand5);
        myWardrobeCheck = findViewById(R.id.check_wardrobe);
        TextView userName = findViewById(R.id.text_user);
        if (MainActivity.user != null)
            userName.setText(MainActivity.user.getFullName());
        TextView tittle = findViewById(R.id.text_title);
        tittle.setText(TITLE);
    }

    private void initList(ArrayList<Product> category) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(recyclerView);
        CreateProductsAdapter adapter = new CreateProductsAdapter(this, category, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onProductClick(int position) {
        Product product;

        if (currentList != null)
            product = currentList.get(position);
        else
            product = products.get(position);

        if (product.getFilterType() == Type.SAPKA) {
            Picasso.get()
                    .load(product.getImage())
                    .into(hat);
            combine.setHat(product);
        } else if ((product.getFilterType() == Type.ELBISE)) {
            upper.setVisibility(View.INVISIBLE);
            upper.setImageBitmap(null);
            bottom.setVisibility(View.INVISIBLE);
            bottom.setImageBitmap(null);
            dress.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(product.getImage())
                    .resize(275, 410)
                    .into(dress);
            if (combine.getBottomGarment() != null)
                combine.setBottomGarment(null);
            if (combine.getUpperGarment() != null)
                combine.setUpperGarment(null);
            combine.setDress(product);
        } else if ((product.getFilterType() == Type.UST)) {
            upper.setVisibility(View.VISIBLE);
            bottom.setVisibility(View.VISIBLE);
            dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(product.getImage())
                    .resize(220, 328)
                    .into(upper);
            if (combine.getDress() != null)
                combine.setDress(null);
            combine.setUpperGarment(product);
        } else if ((product.getFilterType() == Type.CANTA)) {
            Picasso.get()
                    .load(product.getImage())
                    .into(bag);
            combine.setBag(product);
        } else if ((product.getFilterType() == Type.AYAKKABI)) {
            Picasso.get()
                    .load(product.getImage())
                    .resize(149, 149)
                    .into(shoes);
            combine.setShoes(product);
        } else if ((product.getFilterType() == Type.ALT)) {
            upper.setVisibility(View.VISIBLE);
            bottom.setVisibility(View.VISIBLE);
            dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(product.getImage())
                    .resize(220, 328)
                    .into(bottom);
            if (combine.getDress() != null)
                combine.setDress(null);
            combine.setBottomGarment(product);
        } else if ((product.getFilterType() == Type.GOZLUK)) {
            Picasso.get()
                    .load(product.getImage())
                    .into(glasses);
            combine.setGlasses(product);
        }
        Log.d(TAG, "clicked" + position);
    }

    private void fabOpenClose() {
        if (isOpen) {
            fabBag.setAnimation(mFabCloseAnim);
            fabDress.setAnimation(mFabCloseAnim);
            fabGlasses.setAnimation(mFabCloseAnim);
            fabHat.setAnimation(mFabCloseAnim);
            fabPants.setAnimation(mFabCloseAnim);
            fabShirt.setAnimation(mFabCloseAnim);
            fabShoes.setAnimation(mFabCloseAnim);
            fabBag.setVisibility(View.INVISIBLE);
            fabDress.setVisibility(View.INVISIBLE);
            fabGlasses.setVisibility(View.INVISIBLE);
            fabHat.setVisibility(View.INVISIBLE);
            fabPants.setVisibility(View.INVISIBLE);
            fabShirt.setVisibility(View.INVISIBLE);
            fabShoes.setVisibility(View.INVISIBLE);
            fabBag.setClickable(false);
            fabDress.setClickable(false);
            fabGlasses.setClickable(false);
            fabHat.setClickable(false);
            fabPants.setClickable(false);
            fabShirt.setClickable(false);
            fabShoes.setClickable(false);
            isOpen = false;
        } else {
            fabBag.setAnimation(mFabOpenAnim);
            fabDress.setAnimation(mFabOpenAnim);
            fabGlasses.setAnimation(mFabOpenAnim);
            fabHat.setAnimation(mFabOpenAnim);
            fabPants.setAnimation(mFabOpenAnim);
            fabShirt.setAnimation(mFabOpenAnim);
            fabShoes.setAnimation(mFabOpenAnim);
            fabBag.setVisibility(View.VISIBLE);
            fabDress.setVisibility(View.VISIBLE);
            fabGlasses.setVisibility(View.VISIBLE);
            fabHat.setVisibility(View.VISIBLE);
            fabPants.setVisibility(View.VISIBLE);
            fabShirt.setVisibility(View.VISIBLE);
            fabShoes.setVisibility(View.VISIBLE);
            fabBag.setClickable(true);
            fabDress.setClickable(true);
            fabGlasses.setClickable(true);
            fabHat.setClickable(true);
            fabPants.setClickable(true);
            fabShirt.setClickable(true);
            fabShoes.setClickable(true);
            isOpen = true;
        }
    }

    private void categorize() throws JSONException {
        currentList = new ArrayList<>();
        String filterBrands = (String) filterJSON.get("brandFilter");
        String filterMinPrice = (String) filterJSON.get("min");
        String filterMaxPrice = (String) filterJSON.get("max");
        for (Product product : products) {
            String price = product.getPrice().replace(".", "");
            price = price.replace(',', '.');
            price = price.replace('₺', ' ');
            price = price.replace("TL", " ");
            if (myWardrobeCheck.isChecked()) {
                for (Product p : myWardrobe) {
                    if (p.getId().equals(product.getId()) && p.isVisible())
                        currentList.add(product);
                }
            } else {
                if ((filterBrands.equals("") || filterBrands.contains(product.getBrand()))
                        && (filterMinPrice.isEmpty() || Double.parseDouble(filterMinPrice) < Double.parseDouble(price))
                        && (filterMaxPrice.isEmpty() || Double.parseDouble(filterMaxPrice) > Double.parseDouble(price))
                        && (activeType == null || product.getFilterType() == activeType))
                    currentList.add(product);
            }
        }
        initList(currentList);
    }

    private void setImage() {
        if (combine.getDress() != null) {
            upper.setVisibility(View.INVISIBLE);
            bottom.setVisibility(View.INVISIBLE);
            dress.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(combine.getDress().getImage())
                    .into(dress);
        }

        if (combine.getUpperGarment() != null) {
            upper.setVisibility(View.VISIBLE);
            dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(combine.getUpperGarment().getImage())
                    .into(upper);
        }

        if (combine.getBottomGarment() != null) {
            bottom.setVisibility(View.VISIBLE);
            dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(combine.getBottomGarment().getImage())
                    .into(bottom);
        }

        if (combine.getHat() != null) {
            Picasso.get()
                    .load(combine.getHat().getImage())
                    .into(hat);
        }

        if (combine.getShoes() != null) {
            Picasso.get()
                    .load(combine.getShoes().getImage())
                    .resize(149, 149)
                    .into(shoes);
        }

        if (combine.getBag() != null) {
            Picasso.get()
                    .load(combine.getBag().getImage())
                    .into(bag);
        }

        if (combine.getGlasses() != null) {
            Picasso.get()
                    .load(combine.getGlasses().getImage())
                    .into(glasses);
        }
    }

    private void updateCombineDatabase() {
        final String userId = mAuth.getUid();
        Calendar timeNow = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        String combineId = String.valueOf(timeNow.getTimeInMillis());

        assert userId != null;
        mDatabaseReference.child(COMBINES).child(activeCombine.getId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            Combine temp = snapshot.getValue(Combine.class);
                            assert temp != null;
                            if (temp.getUser().equals(MainActivity.user.getFullName())) {
                                mDatabaseReference.child(COMBINES).child(activeCombine.getId()).setValue(combine);
                                mDatabaseReference.child(USERS).child(userId).child(myCOMBINES)
                                        .child(String.valueOf(activeCombine.getId())).setValue(combine);
                            } else {
                                mDatabaseReference.child(COMBINES).child(String.valueOf(combineId)).setValue(combine);
                                mDatabaseReference.child(USERS).child(userId)
                                        .child(myCOMBINES).child(activeCombine.getId()).setValue(null);
                                mDatabaseReference.child(USERS).child(userId).child(myCOMBINES)
                                        .child(String.valueOf(combineId)).setValue(combine);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
}
