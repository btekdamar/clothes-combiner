package com.example.clothescombiner.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.clothescombiner.Adapters.MyCombinesAdapter;
import com.example.clothescombiner.Core.Brands;
import com.example.clothescombiner.Core.Combine;
import com.example.clothescombiner.Core.FashionType;
import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.clothescombiner.Activities.MainActivity.user;
import static com.example.clothescombiner.Activities.SplashScreen.mAuth;
import static com.example.clothescombiner.Activities.SplashScreen.mDatabaseReference;

public class MyCombines extends Activity implements MyCombinesAdapter.OnCombineListener {
    private static final String TAG = "MyCombines";
    private static final String TITLE = "My Combines";
    private static final String USERS = "Users";
    private static final String myCOMBINES = "MyCombines";
    private static final String myWARDROBE = "MyWardrobe";
    private RecyclerView recyclerView;
    private RelativeLayout relativeFilter;
    private RelativeLayout relativeMenu;
    private DrawerLayout drawerLayout;
    private EditText editMin, editMax;
    private TextView warning;
    private AppCompatCheckBox mangoCheck, hmCheck, twistCheck, bershkaCheck, ipekyolCheck, myWardrobeCheck,
            businessCheck, weekendCheck, dinnerCheck, summerCheck, winterCheck, fallCheck, casualCheck;

    private final JSONObject filterJSON = new JSONObject();

    private final ArrayList<Combine> combines = new ArrayList<>();
    private ArrayList<Combine> currentList = new ArrayList<>();
    private final ArrayList<Product> myWardrobe = new ArrayList<>();

    private double currentRating;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_combines);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        initializeById();
        currentRating = 0;
        setWardrobe();
        setList();

        myWardrobeCheck.setOnClickListener(v -> {
            if (myWardrobeCheck.isChecked()) {
                mangoCheck.setClickable(false);
                hmCheck.setClickable(false);
                twistCheck.setClickable(false);
                bershkaCheck.setClickable(false);
                ipekyolCheck.setClickable(false);
            } else {
                mangoCheck.setClickable(true);
                hmCheck.setClickable(true);
                twistCheck.setClickable(true);
                bershkaCheck.setClickable(true);
                ipekyolCheck.setClickable(true);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void ClickMenu(View view) {
        relativeMenu.bringToFront();
        relativeMenu.setVisibility(View.VISIBLE);
        relativeFilter.setVisibility(View.GONE);
        openDrawer(drawerLayout);
    }

    public void FilterClick(View view) {
        Button applyButton = findViewById(R.id.button_apply);
        applyButton.setVisibility(View.VISIBLE);
        applyButton.bringToFront();
        relativeFilter.bringToFront();
        relativeFilter.setVisibility(View.VISIBLE);
        relativeMenu.setVisibility(View.GONE);
        RelativeLayout fashionBox = findViewById(R.id.fashion_section);
        fashionBox.setVisibility(View.VISIBLE);
        openDrawer(drawerLayout);
    }

    public void HomeClick(View view) {
        Log.d(TAG, "onClick: Home");
        redirectActivity(MyCombines.this, MainActivity.class);
        finish();

    }

    public void MyWardrobeClick(View view) {
        Log.d(TAG, "onClick: myWardrobe");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(MyCombines.this, MyWardrobe.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void MyCombineClick(View view) {
        Log.d(TAG, "onClick: myCombines");
    }

    public void CreateCombineClick(View view) {
        Log.d(TAG, "onClick: createCombine");
        finish();
        redirectActivity(MyCombines.this, CreateCombine.class);
    }

    public void OneRatingClick(View view) {
        currentRating = 1;
    }

    public void TwoRatingClick(View view) {
        currentRating = 2;
    }

    public void ThreeRatingClick(View view) {
        currentRating = 3;
    }

    public void FourRatingClick(View view) {
        currentRating = 4;
    }

    public void ApplyClick(View view) throws JSONException {
        setFilter();
        editMin.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideKeyboard(v);
        });

        editMax.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideKeyboard(v);
        });

        closeDrawer(drawerLayout);
    }

    public void LogOutClick(View view) {
        Log.d(TAG, "onClick: logOut");
        user = null;
        mAuth.signOut();
        finish();
        redirectActivity(MyCombines.this, MainActivity.class);
    }

    private void setFilter() throws JSONException {
        String minPrice = editMin.getText().toString().trim();
        String maxPrice = editMax.getText().toString().trim();
        try {
            filterJSON.put("brandFilter", "");
            if (minPrice.isEmpty())
                filterJSON.put("min", "");
            else
                filterJSON.put("min", minPrice);
            if (maxPrice.isEmpty())
                filterJSON.put("max", "");
            else
                filterJSON.put("max", maxPrice);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String brandFilter = "", fashionFilter = "";

        if (!myWardrobeCheck.isChecked()) {
            if (mangoCheck.isChecked())
                brandFilter += Brands.MANGO.toString();
            if (hmCheck.isChecked())
                brandFilter += Brands.HM.toString();
            if (twistCheck.isChecked())
                brandFilter += Brands.TWIST.toString();
            if (bershkaCheck.isChecked())
                brandFilter += Brands.BERSHKA.toString();
            if (ipekyolCheck.isChecked())
                brandFilter += Brands.IPEKYOL.toString();
        }

        if (businessCheck.isChecked())
            fashionFilter += FashionType.BUSINESS.toString();
        if (weekendCheck.isChecked())
            fashionFilter += FashionType.WEEKEND.toString();
        if (dinnerCheck.isChecked())
            fashionFilter += FashionType.DINNER.toString();
        if (summerCheck.isChecked())
            fashionFilter += FashionType.SUMMER.toString();
        if (winterCheck.isChecked())
            fashionFilter += FashionType.WINTER.toString();
        if (fallCheck.isChecked())
            fashionFilter += FashionType.FALL.toString();
        if (casualCheck.isChecked())
            fashionFilter += FashionType.CASUAL.toString();

        try {
            filterJSON.put("brandFilter", brandFilter);
            filterJSON.put("fashionFilter", fashionFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        categorize();
    }

    private static void redirectActivity(Activity activity, Class<?> aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void initList(ArrayList<Combine> category) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(recyclerView);
        MyCombinesAdapter adapter = new MyCombinesAdapter(this, category, this, true);
        recyclerView.setAdapter(adapter);
    }

    private void initializeById() {
        recyclerView = findViewById(R.id.list_combines);
        drawerLayout = findViewById(R.id.drawer_layout);
        relativeFilter = findViewById(R.id.relative_filter);
        relativeMenu = findViewById(R.id.relative_menu);
        recyclerView = findViewById(R.id.list_combines);
        editMin = findViewById(R.id.edit_min);
        editMax = findViewById(R.id.edit_max);
        mangoCheck = findViewById(R.id.check_brand1);
        hmCheck = findViewById(R.id.check_brand2);
        twistCheck = findViewById(R.id.check_brand3);
        bershkaCheck = findViewById(R.id.check_brand4);
        ipekyolCheck = findViewById(R.id.check_brand5);
        businessCheck = findViewById(R.id.check_fashion1);
        weekendCheck = findViewById(R.id.check_fashion2);
        dinnerCheck = findViewById(R.id.check_fashion3);
        summerCheck = findViewById(R.id.check_fashion4);
        winterCheck = findViewById(R.id.check_fashion5);
        fallCheck = findViewById(R.id.check_fashion6);
        casualCheck = findViewById(R.id.check_fashion7);
        myWardrobeCheck = findViewById(R.id.check_wardrobe);
        warning = findViewById(R.id.text_warning);
        TextView textUser = findViewById(R.id.text_user);
        RelativeLayout categoryBox = findViewById(R.id.category_section);
        categoryBox.setVisibility(View.GONE);
        TextView title = findViewById(R.id.text_title);
        title.setText(TITLE);
        if (user != null)
            textUser.setText(user.getFullName());
    }

    private void categorize() throws JSONException {
        currentList = new ArrayList<>();

        String filterBrands = (String) filterJSON.get("brandFilter");
        String filterFashion = (String) filterJSON.get("fashionFilter");
        String filterMinPrice = (String) filterJSON.get("min");
        String filterMaxPrice = (String) filterJSON.get("max");
        if (filterBrands.equals("") && filterFashion.equals("") && filterMinPrice.equals("") && filterMaxPrice.equals("") && combines.size() != 0 && !myWardrobeCheck.isChecked()) {
            recyclerView.setVisibility(View.VISIBLE);
            warning.setVisibility(View.GONE);
            initList(combines);
            return;
        } else {
            for (Combine combine : combines) {
                ArrayList<FashionType> fashionTypes = combine.getFashionTypes();
                boolean controlBrand;
                boolean controlFashion = false;

                if (filterFashion.equals(""))
                    controlFashion = true;
                else {
                    if (fashionTypes != null) {
                        for (FashionType fashionType : fashionTypes) {
                            if (filterFashion.contains(fashionType.toString())) {
                                controlFashion = true;
                                break;
                            }
                        }
                    }
                }

                if (myWardrobeCheck.isChecked()) {
                    boolean isInMyWardrobe;
                    for (Product p : myWardrobe) {
                        if (combine.getBag() != null && p.getId().equals(combine.getBag().getId()))
                            isInMyWardrobe = true;
                        else if (combine.getBottomGarment() != null && p.getId().equals(combine.getBottomGarment().getId()))
                            isInMyWardrobe = true;
                        else if (combine.getDress() != null && p.getId().equals(combine.getDress().getId()))
                            isInMyWardrobe = true;
                        else if (combine.getGlasses() != null && p.getId().equals(combine.getGlasses().getId()))
                            isInMyWardrobe = true;
                        else if (combine.getShoes() != null && p.getId().equals(combine.getShoes().getId()))
                            isInMyWardrobe = true;
                        else if (combine.getUpperGarment() != null && p.getId().equals(combine.getUpperGarment().getId()))
                            isInMyWardrobe = true;
                        else
                            isInMyWardrobe = combine.getHat() != null && p.getId().equals(combine.getHat().getId());

                        if (isInMyWardrobe && controlFashion
                                && (filterMinPrice.isEmpty() || Double.parseDouble(filterMinPrice) < combine.getPrice())
                                && (filterMaxPrice.isEmpty() || Double.parseDouble(filterMaxPrice) > combine.getPrice())
                                && (currentRating <= combine.getRate())) {
                            currentList.add(combine);
                            break;
                        }
                    }
                } else {
                    if (filterBrands.equals(""))
                        controlBrand = true;
                    else {
                        if (combine.getBag() != null && filterBrands.contains(combine.getBag().getBrand()))
                            controlBrand = true;
                        else if (combine.getBottomGarment() != null && filterBrands.contains(combine.getBottomGarment().getBrand()))
                            controlBrand = true;
                        else if (combine.getDress() != null && filterBrands.contains(combine.getDress().getBrand()))
                            controlBrand = true;
                        else if (combine.getGlasses() != null && filterBrands.contains(combine.getGlasses().getBrand()))
                            controlBrand = true;
                        else if (combine.getShoes() != null && filterBrands.contains(combine.getShoes().getBrand()))
                            controlBrand = true;
                        else if (combine.getUpperGarment() != null && filterBrands.contains(combine.getUpperGarment().getBrand()))
                            controlBrand = true;
                        else
                            controlBrand = combine.getHat() != null && filterBrands.contains(combine.getHat().getBrand());
                    }
                    if (controlBrand && controlFashion
                            && (filterMinPrice.isEmpty() || Double.parseDouble(filterMinPrice) < combine.getPrice())
                            && (filterMaxPrice.isEmpty() || Double.parseDouble(filterMaxPrice) > combine.getPrice())
                            && (currentRating <= combine.getRate()))
                        currentList.add(combine);
                }
            }
        }

        if (currentList.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);
            warning.setVisibility(View.GONE);
            initList(currentList);
        } else {
            recyclerView.setVisibility(View.GONE);
            warning.setVisibility(View.VISIBLE);
        }
    }

    private void setList() {
        String userId = mAuth.getUid();
        assert userId != null;
        mDatabaseReference.child(USERS).child(userId).child(myCOMBINES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        final Combine readCombine = ds.getValue(Combine.class);
                        assert readCombine != null;
                        readCombine.setId(ds.getKey());
                        combines.add(readCombine);
                    }
                }
                initList(combines);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setWardrobe() {
        String userId = mAuth.getUid();
        assert userId != null;
        mDatabaseReference.child(USERS).child(userId).child(myWARDROBE).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        final Product readProduct = ds.getValue(Product.class);
                        assert readProduct != null;
                        myWardrobe.add(readProduct);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onCombineClick(int position) {
        if (currentList.size() != 0) {
            Combine selectedCombine = currentList.get(position);
            Intent myIntent = new Intent(MyCombines.this, CombineDetails.class);
            myIntent.putExtra("Combine", selectedCombine);
            startActivity(myIntent);
        } else {
            Combine selectedCombine = combines.get(position);
            Intent myIntent = new Intent(MyCombines.this, CombineDetails.class);
            myIntent.putExtra("Combine", selectedCombine);
            startActivity(myIntent);
        }
    }


}
