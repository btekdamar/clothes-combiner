package com.example.clothescombiner.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.clothescombiner.Core.Combine;
import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.Core.Type;
import com.example.clothescombiner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Objects;

import static com.example.clothescombiner.Activities.MainActivity.user;
import static com.example.clothescombiner.Activities.SplashScreen.mAuth;
import static com.example.clothescombiner.Activities.SplashScreen.mDatabaseReference;

public class CombineDetails extends Activity {
    private static final String TAG = "CombineDetails";
    private static final String USERS = "Users";
    private static final String COMBINES = "Combines";
    private static final String myCOMBINES = "MyCombines";
    private RelativeLayout relativeMenu;
    private RelativeLayout relativeFilter;
    private LinearLayout linearLogout;
    private LinearLayout linearLogin;
    private ImageView dress, upper, bottom, shoes, bag, hat, glasses;
    private RatingBar rating;
    private TextView textUser;
    private TextView textRate;
    private DrawerLayout drawerLayout;
    private Combine activeCombine;
    private Boolean productsButtonActive = false;
    private Boolean control = false;
    private long count = 0;
    private Double oldRating;

    private static void redirectActivity(Activity activity, Class<?> aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.combine_details);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        Intent i = getIntent();
        activeCombine = (Combine) i.getSerializableExtra("Combine");
        initializeById();
        setImage();

        rating.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (user != null) {
            linearLogin.setVisibility(View.VISIBLE);
            linearLogout.setVisibility(View.INVISIBLE);
            textUser.setText(user.getFullName());
        }

        if (user == null) {
            linearLogin.setVisibility(View.INVISIBLE);
            linearLogout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void ClickMenu(View view) {
        relativeMenu.bringToFront();
        relativeMenu.setVisibility(View.VISIBLE);
        relativeFilter.setVisibility(View.GONE);
        openDrawer(drawerLayout);
    }

    public void HomeClick(View view) {
        Log.d(TAG, "onClick: Home");
        redirectActivity(CombineDetails.this, MainActivity.class);
        finish();
    }

    public void MyCombineClick(View view) {
        Log.d(TAG, "onClick: myCombines");
        if (mAuth.getCurrentUser() != null) {
            redirectActivity(CombineDetails.this, MyCombines.class);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void CreateCombineClick(View view) {
        Log.d(TAG, "onClick: createCombine");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(CombineDetails.this, CreateCombine.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void MyWardrobeClick(View view) {
        Log.d(TAG, "onClick: myWardrobe");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(CombineDetails.this, MyWardrobe.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void SaveToolbarClick(View view) {
        Log.d(TAG, "onClick: saveToolbarClick");
        final String userId = mAuth.getUid();
        final String combineId = activeCombine.getId();
        assert userId != null;
        mDatabaseReference.child(USERS).child(userId)
                .child(myCOMBINES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean control = false;
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        String readId = ds.getKey();
                        assert readId != null;
                        if (readId.equals(combineId)) {
                            control = true;
                            Toast.makeText(getApplicationContext(),
                                    "This combine is already included in your combines.",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                    if (!control) {
                        mDatabaseReference.child(USERS).child(userId).child(myCOMBINES)
                                .child(combineId).setValue(activeCombine);
                        Toast.makeText(getApplicationContext(),
                                "Combine added.",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mDatabaseReference.child(USERS).child(userId).child(myCOMBINES)
                            .child(combineId).setValue(activeCombine);
                    Toast.makeText(getApplicationContext(),
                            "Combine added.",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void PriceTagClick(View view) {
        Log.d(TAG, "onClick: priceTag");
        if (!productsButtonActive) {
            if (activeCombine.getGlasses() != null)
                seeProduct(Type.GOZLUK, activeCombine.getGlasses());
            if (activeCombine.getUpperGarment() != null)
                seeProduct(Type.UST, activeCombine.getUpperGarment());
            if (activeCombine.getDress() != null)
                seeProduct(Type.ELBISE, activeCombine.getDress());
            if (activeCombine.getBottomGarment() != null)
                seeProduct(Type.ALT, activeCombine.getBottomGarment());
            if (activeCombine.getHat() != null)
                seeProduct(Type.SAPKA, activeCombine.getHat());
            if (activeCombine.getBag() != null)
                seeProduct(Type.CANTA, activeCombine.getBag());
            if (activeCombine.getShoes() != null)
                seeProduct(Type.AYAKKABI, activeCombine.getShoes());
        } else
            popupClose();
    }

    public void VoteClick(View view) {
        Log.d(TAG, "onClick: vote");
        final String id = mAuth.getUid();
        assert id != null;
        mDatabaseReference.child(COMBINES).child(activeCombine.getId())
                .child("Vote").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        if (Objects.equals(ds.getKey(), id)) {
                            control = true;
                            oldRating = ds.getValue(Double.class);
                            break;
                        }
                    }
                    count = snapshot.getChildrenCount();
                    double rate;
                    if (control) {
                        rate = (activeCombine.getRate() * count - oldRating);
                        rate += rating.getRating();
                        rate /= count;
                    } else {
                        rate = activeCombine.getRate() * count;
                        rate += rating.getRating();
                        rate /= (count + 1);
                    }
                    activeCombine.setRate(rate);
                    DecimalFormat df = new DecimalFormat("#.#");
                    df.setRoundingMode(RoundingMode.CEILING);
                    textRate.setText((df.format(rate)));
                } else {
                    double rate;
                    rate = activeCombine.getRate() * count;
                    rate += rating.getRating();
                    rate /= (count + 1);
                    activeCombine.setRate(rate);
                    DecimalFormat df = new DecimalFormat("#.#");
                    df.setRoundingMode(RoundingMode.CEILING);
                    textRate.setText(df.format(rate));
                }
                mDatabaseReference.child(COMBINES).child(activeCombine.getId())
                        .child("Vote").child(id).setValue(rating.getRating());
                mDatabaseReference.child(COMBINES).child(activeCombine.getId())
                        .child("rate").setValue(activeCombine.getRate());
                mDatabaseReference.child(USERS).child(id).child(myCOMBINES)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    for (DataSnapshot ds : snapshot.getChildren()) {
                                        if (Objects.equals(ds.getKey(), activeCombine.getId())) {
                                            mDatabaseReference.child(USERS).child(id)
                                                    .child(myCOMBINES).child(activeCombine.getId())
                                                    .child("rate").setValue(activeCombine.getRate());
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void LogOutClick(View view) {
        user = null;
        mAuth.signOut();
        finish();
        Intent myIntent = new Intent(CombineDetails.this, CombineDetails.class);
        myIntent.putExtra("Combine", activeCombine);
        startActivity(myIntent);
    }

    public void LogInClick(View view) {
        Log.d(TAG, "onClick: logIn");
        finish();
        redirectActivity(CombineDetails.this, Login.class);
    }

    public void SignUpClick(View view) {
        Log.d(TAG, "onClick: signUp");
        finish();
        redirectActivity(CombineDetails.this, Register.class);
    }

    private void initializeById() {
        relativeMenu = findViewById(R.id.relative_menu);
        relativeFilter = findViewById(R.id.relative_filter);
        drawerLayout = findViewById(R.id.drawer_layout);
        linearLogin = findViewById(R.id.linear_login);
        linearLogout = findViewById(R.id.linear_logout);
        textUser = findViewById(R.id.text_user);
        dress = findViewById(R.id.image_dress);
        upper = findViewById(R.id.image_upperGarment);
        bottom = findViewById(R.id.image_bottomGarment);
        shoes = findViewById(R.id.image_shoes);
        bag = findViewById(R.id.image_bag);
        hat = findViewById(R.id.image_hat);
        glasses = findViewById(R.id.image_glasses);
        rating = findViewById(R.id.rating_rate);
        textRate = findViewById(R.id.text_rate);
        assert activeCombine != null;
        rating.setRating((float) activeCombine.getRate());
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        textRate.setText(df.format(activeCombine.getRate()));
        if (MainActivity.user != null)
            textUser.setText(MainActivity.user.getFullName());
        TextView tittle = findViewById(R.id.text_title);
        tittle.setText(activeCombine.getName());
        ImageView filterButton = findViewById(R.id.button_filter);
        filterButton.setVisibility(View.GONE);
        TextView textSave = findViewById(R.id.text_save);
        textSave.setVisibility(View.VISIBLE);
        ImageView priceButton = findViewById(R.id.button_price_tag);
        priceButton.setVisibility(View.VISIBLE);
    }

    private void setImage() {
        if (activeCombine.getDress() != null) {
            upper.setVisibility(View.INVISIBLE);
            bottom.setVisibility(View.INVISIBLE);
            dress.setVisibility(View.VISIBLE);
            Picasso.get()
                    .load(activeCombine.getDress().getImage())
                    .into(dress);
        }

        if (activeCombine.getUpperGarment() != null) {
            upper.setVisibility(View.VISIBLE);
            dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(activeCombine.getUpperGarment().getImage())
                    .into(upper);
        }

        if (activeCombine.getBottomGarment() != null) {
            bottom.setVisibility(View.VISIBLE);
            dress.setVisibility(View.INVISIBLE);
            Picasso.get()
                    .load(activeCombine.getBottomGarment().getImage())
                    .into(bottom);
        }

        if (activeCombine.getHat() != null) {
            Picasso.get()
                    .load(activeCombine.getHat().getImage())
                    .resize(80, 80)
                    .into(hat);
        }

        if (activeCombine.getShoes() != null) {
            Picasso.get()
                    .load(activeCombine.getShoes().getImage())
                    .resize(149, 149)
                    .into(shoes);
        }

        if (activeCombine.getBag() != null) {
            Picasso.get()
                    .load(activeCombine.getBag().getImage())
                    .into(bag);
        }

        if (activeCombine.getGlasses() != null) {
            Picasso.get()
                    .load(activeCombine.getGlasses().getImage())
                    .into(glasses);
        }
    }

    private void seeProduct(Type type, final Product product) {
        productsButtonActive = true;
        if (type == Type.GOZLUK) {
            View includeLayout = findViewById(R.id.popup_glasses);
            TextView brand = includeLayout.findViewById(R.id.text_brand);
            TextView price = includeLayout.findViewById(R.id.text_price);
            ImageButton productButton = includeLayout.findViewById(R.id.button_product);
            brand.setText(product.getBrand());
            price.setText(product.getPrice());
            includeLayout.setVisibility(View.VISIBLE);
            includeLayout.bringToFront();
            productButton.setOnClickListener(v -> {
                Intent myIntent = new Intent(CombineDetails.this, ProductDetail.class);
                myIntent.putExtra("Product", product);
                startActivity(myIntent);
            });

        }
        if (type == Type.UST) {
            View includeLayout = findViewById(R.id.popup_upper);
            TextView brand = includeLayout.findViewById(R.id.text_brand);
            TextView price = includeLayout.findViewById(R.id.text_price);
            ImageButton productButton = includeLayout.findViewById(R.id.button_product);
            brand.setText(product.getBrand());
            price.setText(product.getPrice());
            includeLayout.setVisibility(View.VISIBLE);
            includeLayout.bringToFront();
            productButton.setOnClickListener(v -> {
                Intent myIntent = new Intent(CombineDetails.this, ProductDetail.class);
                myIntent.putExtra("Product", product);
                startActivity(myIntent);
            });
        }
        if (type == Type.ELBISE) {
            View includeLayout = findViewById(R.id.popup_upper);
            TextView brand = includeLayout.findViewById(R.id.text_brand);
            TextView price = includeLayout.findViewById(R.id.text_price);
            ImageButton productButton = includeLayout.findViewById(R.id.button_product);
            brand.setText(product.getBrand());
            price.setText(product.getPrice());
            includeLayout.setVisibility(View.VISIBLE);
            includeLayout.bringToFront();
            productButton.setOnClickListener(v -> {
                Intent myIntent = new Intent(CombineDetails.this, ProductDetail.class);
                myIntent.putExtra("Product", product);
                startActivity(myIntent);
            });
        }
        if (type == Type.ALT) {
            View includeLayout = findViewById(R.id.popup_bottom);
            TextView brand = includeLayout.findViewById(R.id.text_brand);
            TextView price = includeLayout.findViewById(R.id.text_price);
            ImageButton productButton = includeLayout.findViewById(R.id.button_product);
            brand.setText(product.getBrand());
            price.setText(product.getPrice());
            includeLayout.setVisibility(View.VISIBLE);
            includeLayout.bringToFront();
            productButton.setOnClickListener(v -> {
                Intent myIntent = new Intent(CombineDetails.this, ProductDetail.class);
                myIntent.putExtra("Product", product);
                startActivity(myIntent);
            });
        }
        if (type == Type.SAPKA) {
            View includeLayout = findViewById(R.id.popup_hat);
            TextView brand = includeLayout.findViewById(R.id.text_brand);
            TextView price = includeLayout.findViewById(R.id.text_price);
            ImageButton productButton = includeLayout.findViewById(R.id.button_product);
            brand.setText(product.getBrand());
            price.setText(product.getPrice());
            includeLayout.setVisibility(View.VISIBLE);
            includeLayout.bringToFront();
            productButton.setOnClickListener(v -> {
                Intent myIntent = new Intent(CombineDetails.this, ProductDetail.class);
                myIntent.putExtra("Product", product);
                startActivity(myIntent);
            });
        }
        if (type == Type.CANTA) {
            View includeLayout = findViewById(R.id.popup_bag);
            TextView brand = includeLayout.findViewById(R.id.text_brand);
            TextView price = includeLayout.findViewById(R.id.text_price);
            ImageButton productButton = includeLayout.findViewById(R.id.button_product);
            brand.setText(product.getBrand());
            price.setText(product.getPrice());
            includeLayout.setVisibility(View.VISIBLE);
            includeLayout.bringToFront();
            productButton.setOnClickListener(v -> {
                Intent myIntent = new Intent(CombineDetails.this, ProductDetail.class);
                myIntent.putExtra("Product", product);
                startActivity(myIntent);
            });
        }
        if (type == Type.AYAKKABI) {
            View includeLayout = findViewById(R.id.popup_shoes);
            TextView brand = includeLayout.findViewById(R.id.text_brand);
            TextView price = includeLayout.findViewById(R.id.text_price);
            ImageButton productButton = includeLayout.findViewById(R.id.button_product);
            brand.setText(product.getBrand());
            price.setText(product.getPrice());
            includeLayout.setVisibility(View.VISIBLE);
            includeLayout.bringToFront();
            productButton.setOnClickListener(v -> {
                Intent myIntent = new Intent(CombineDetails.this, ProductDetail.class);
                myIntent.putExtra("Product", product);
                startActivity(myIntent);
            });
        }
    }

    private void popupClose() {
        View includeLayout = findViewById(R.id.popup_shoes);
        includeLayout.setVisibility(View.GONE);
        includeLayout.bringToFront();
        includeLayout = findViewById(R.id.popup_bag);
        includeLayout.setVisibility(View.GONE);
        includeLayout.bringToFront();
        includeLayout = findViewById(R.id.popup_upper);
        includeLayout.setVisibility(View.GONE);
        includeLayout.bringToFront();
        includeLayout = findViewById(R.id.popup_bottom);
        includeLayout.setVisibility(View.GONE);
        includeLayout.bringToFront();
        includeLayout = findViewById(R.id.popup_hat);
        includeLayout.setVisibility(View.GONE);
        includeLayout.bringToFront();
        includeLayout = findViewById(R.id.popup_glasses);
        includeLayout.setVisibility(View.GONE);
        includeLayout.bringToFront();
        productsButtonActive = false;
    }
}
