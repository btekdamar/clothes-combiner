package com.example.clothescombiner.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.clothescombiner.R;
import com.example.clothescombiner.Core.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import static com.example.clothescombiner.Activities.SplashScreen.mDatabaseReference;
import static com.example.clothescombiner.Activities.SplashScreen.mAuth;

public class ForgotPassword extends Activity {
    private final static String TAG = "ForgotPassword";
    private final static String USER = "Users";

    private boolean control;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_screen);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        final EditText forgot = findViewById(R.id.edit_forgotmail);
        final Button sendButton = findViewById(R.id.button_send);

        sendButton.setOnClickListener(v -> {
            Log.d(TAG, "onClick: sendButton");
            control = false;
            final String forgotMail = forgot.getText().toString();
            mDatabaseReference.child(USER).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                        User user = dataSnapshot.getValue(User.class);
                        if (user != null && user.getEmail().equals(forgotMail))
                            control = true;
                    }

                    if (!control) {
                        Log.d(TAG, "findEmail: failure");
                        toastMessage("Email not found!");
                    } else {
                        Log.d(TAG, "findEmail: success");
                        mAuth.sendPasswordResetEmail(forgotMail);
                        toastMessage("Email is send");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void toastMessage(String message) {
        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
    }
}
