package com.example.clothescombiner.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.clothescombiner.Core.Gender;
import com.example.clothescombiner.Core.User;
import com.example.clothescombiner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.util.Objects;

import static com.example.clothescombiner.Activities.SplashScreen.mAuth;
import static com.example.clothescombiner.Activities.SplashScreen.mDatabaseReference;

public class Register extends Activity {
    private static final String TAG = "Register";
    private static final String USERS = "Users";

    private EditText name;
    private EditText email;
    private EditText password;
    private RadioButton man;
    private ProgressBar progressBar;

    private User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        name = findViewById(R.id.edit_name);
        email = findViewById(R.id.edit_email);
        password = findViewById(R.id.edit_password);
        man = findViewById(R.id.radio_male);


        progressBar = findViewById(R.id.progressBar);

        Button sign = findViewById(R.id.button_sign);
        sign.setOnClickListener(v -> {
            Log.d(TAG, "onClick: Submit pressed");

            String nm = name.getText().toString().trim();
            String mail = email.getText().toString().trim();
            String pass = password.getText().toString().trim();
            Gender userGender;

            if (TextUtils.isEmpty(mail)) {
                email.setError("Email is required");
                return;
            }

            if (TextUtils.isEmpty(pass)) {
                password.setError("Password is Required");
                return;
            }

            if (TextUtils.isEmpty(nm)) {
                email.setError("Name is required");
                return;
            }

            if (nm.length() > 15) {
                name.setError("Username must be maximum 15 characters");
                return;
            }

            if (pass.length() < 6) {
                password.setError("Password must be minimum 6 characters");
                return;
            }
            if (man.isChecked())
                userGender = Gender.MAN;
            else
                userGender = Gender.WOMAN;

            progressBar.setVisibility(View.VISIBLE);
            user = new User(nm, mail, userGender.toString());
            authRegister(mail, pass);
        });

        TextView logIn = findViewById(R.id.text_login);
        logIn.setOnClickListener(v -> {
            finish();
            Intent myIntent = new Intent(Register.this, Login.class);
            startActivity(myIntent);
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    private void authRegister(String mail, String pass) {
        mAuth.createUserWithEmailAndPassword(mail, pass)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createUserWithEmail:success");
                        Objects.requireNonNull(mAuth.getCurrentUser()).sendEmailVerification()
                                .addOnCompleteListener(task1 -> {
                                    if (task1.isSuccessful()) {
                                        progressBar.setVisibility(View.INVISIBLE);
                                        toastMessage("Registered successfully. Verification mail send. " +
                                                "Please check your email.");
                                        updateUserDatabase();
                                    } else {
                                        progressBar.setVisibility(View.INVISIBLE);
                                        Log.w(TAG, "createUserWithEmail:failure", task1.getException());
                                        toastMessage(Objects.requireNonNull(task1.getException()).getMessage());
                                    }
                                });
                    } else {
                        progressBar.setVisibility(View.INVISIBLE);
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        toastMessage(Objects.requireNonNull(task.getException()).getMessage());
                    }
                });
    }

    private void toastMessage(String message) {
        Toast.makeText(Register.this, message, Toast.LENGTH_SHORT).show();
    }

    private void updateUserDatabase() {
        String keyId = mAuth.getUid();
        assert keyId != null;
        DatabaseReference ref = mDatabaseReference.child(USERS).child(keyId);
        ref.runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData currentData) {
                if (currentData.getValue() == null) {
                    currentData.setValue(user);
                    return Transaction.success(currentData);
                }
                return Transaction.abort();
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                Log.d(TAG, "runTransaction:onComplete:" + databaseError);
            }
        });
        finish();
        Intent loginIntent = new Intent(this, Login.class);
        startActivity(loginIntent);
    }
}
