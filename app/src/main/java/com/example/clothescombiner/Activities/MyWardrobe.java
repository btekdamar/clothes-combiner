package com.example.clothescombiner.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.clothescombiner.Adapters.MyWardrobeAdapter;
import com.example.clothescombiner.Adapters.ProductsAdapter;
import com.example.clothescombiner.Core.Brands;
import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.Core.Type;
import com.example.clothescombiner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.clothescombiner.Activities.MainActivity.products;
import static com.example.clothescombiner.Activities.MainActivity.user;
import static com.example.clothescombiner.Activities.SplashScreen.mAuth;
import static com.example.clothescombiner.Activities.SplashScreen.mDatabaseReference;

public class MyWardrobe extends AppCompatActivity implements MyWardrobeAdapter.OnProductListener, ProductsAdapter.OnProductListener {
    private static final String TAG = "MyWardrobe";
    private static final String TITLE = "My Wardrobe";
    private static final String USERS = "Users";
    private static final String myWARDROBE = "MyWardrobe";
    private ArrayList<Product> myProducts = new ArrayList<>();
    private ArrayList<Product> currentList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RelativeLayout relativeFilter;
    private RelativeLayout relativeMenu;
    private RelativeLayout relativeDrawer;
    private DrawerLayout drawerLayout;
    private EditText editMin, editMax;
    private TextView warning, backWardrobe;
    private ImageView addProductWardrobe;
    private AppCompatCheckBox mangoCheck, hmCheck, twistCheck, bershkaCheck, ipekyolCheck, myWardrobeCheck,
            hatCheck, bagCheck, glassesCheck, shoesCheck, upperCheck, bottomCheck, dressCheck;
    private boolean isMyWardrobe = true;

    private final JSONObject filterJSON = new JSONObject();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_wardrobe);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        initializeById();

        setList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isMyWardrobe) {
            addProductWardrobe.setVisibility(View.VISIBLE);
            backWardrobe.setVisibility(View.GONE);
        } else {
            addProductWardrobe.setVisibility(View.GONE);
            backWardrobe.setVisibility(View.VISIBLE);
        }
    }

    public void ClickMenu(View view) {
        relativeMenu.bringToFront();
        relativeMenu.setVisibility(View.VISIBLE);
        relativeFilter.setVisibility(View.GONE);
        ViewGroup.LayoutParams params = relativeDrawer.getLayoutParams();
        float density = this.getResources().getDisplayMetrics().density;
        float px = 250 * density;
        params.width = (int) px;
        relativeDrawer.setLayoutParams(params);
        openDrawer(drawerLayout);
    }

    public void FilterClick(View view) {
        RelativeLayout fashionBox = findViewById(R.id.relative_fashion_category);
        fashionBox.setVisibility(View.VISIBLE);
        RelativeLayout ratingsBox = findViewById(R.id.ratings_box);
        ratingsBox.setVisibility(View.GONE);
        RelativeLayout wardrobeBox = findViewById(R.id.wardrobe_box);
        wardrobeBox.setVisibility(View.GONE);
        Button applyButton = findViewById(R.id.button_apply);
        applyButton.setVisibility(View.VISIBLE);
        applyButton.bringToFront();
        relativeFilter.bringToFront();
        relativeFilter.setVisibility(View.VISIBLE);
        relativeMenu.setVisibility(View.GONE);
        ViewGroup.LayoutParams params = relativeDrawer.getLayoutParams();
        float density = this.getResources().getDisplayMetrics().density;
        float px = 250 * density;
        params.width = (int) px;
        relativeDrawer.setLayoutParams(params);
        openDrawer(drawerLayout);
    }

    public void ApplyClick(View view) throws JSONException {
        setFilter();
        editMin.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideKeyboard(v);
        });

        editMax.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus)
                hideKeyboard(v);
        });

        closeDrawer(drawerLayout);
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setFilter() throws JSONException {
        String minPrice = editMin.getText().toString().trim();
        String maxPrice = editMax.getText().toString().trim();
        try {
            filterJSON.put("brandFilter", "");
            if (minPrice.isEmpty())
                filterJSON.put("min", "");
            else
                filterJSON.put("min", minPrice);
            if (maxPrice.isEmpty())
                filterJSON.put("max", "");
            else
                filterJSON.put("max", maxPrice);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String brandFilter = "", fashionFilter = "", categoryFilter = "";

        if (mangoCheck.isChecked())
            brandFilter += Brands.MANGO.toString();
        if (hmCheck.isChecked())
            brandFilter += Brands.HM.toString();
        if (twistCheck.isChecked())
            brandFilter += Brands.TWIST.toString();
        if (bershkaCheck.isChecked())
            brandFilter += Brands.BERSHKA.toString();
        if (ipekyolCheck.isChecked())
            brandFilter += Brands.IPEKYOL.toString();

        if (hatCheck.isChecked())
            categoryFilter += Type.SAPKA.toString();
        if (dressCheck.isChecked())
            categoryFilter += Type.ELBISE.toString();
        if (upperCheck.isChecked())
            categoryFilter += Type.UST.toString();
        if (bottomCheck.isChecked())
            categoryFilter += Type.ALT.toString();
        if (glassesCheck.isChecked())
            categoryFilter += Type.GOZLUK.toString();
        if (shoesCheck.isChecked())
            categoryFilter += Type.AYAKKABI.toString();
        if (bagCheck.isChecked())
            categoryFilter += Type.CANTA.toString();

        try {
            filterJSON.put("brandFilter", brandFilter);
            filterJSON.put("fashionFilter", fashionFilter);
            filterJSON.put("categoryFilter", categoryFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (isMyWardrobe)
            categorizeProduct();
        else
            categorizeProductWardrobe();
    }

    public void HomeClick(View view) {
        Log.d(TAG, "onClick: Home");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(MyWardrobe.this, MainActivity.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void MyCombineClick(View view) {
        Log.d(TAG, "onClick: myCombines");
        if (mAuth.getCurrentUser() != null) {
            redirectActivity(MyWardrobe.this, MyCombines.class);
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void MyWardrobeClick(View view) {
        Log.d(TAG, "onClick: myWardrobe");
    }

    public void CreateCombineClick(View view) {
        Log.d(TAG, "onClick: createCombine");
        if (mAuth.getCurrentUser() != null) {
            // finish();
            redirectActivity(MyWardrobe.this, CreateCombine.class);
        } else {
            Toast.makeText(getApplicationContext(), "Please Log in", Toast.LENGTH_SHORT).show();
        }
    }

    public void AddProductToWardrobe(View view) {
        Log.d(TAG, "onClick: addProductToWardrobe");
        resetFilter();
        getProductStartScreen();
        isMyWardrobe = false;
        addProductWardrobe.setVisibility(View.GONE);
        backWardrobe.setVisibility(View.VISIBLE);
    }

    public void BackWardrobe(View view) {
        Log.d(TAG, "onClick: addProductToWardrobe");
        resetFilter();
        isMyWardrobe = true;
        addProductWardrobe.setVisibility(View.VISIBLE);
        backWardrobe.setVisibility(View.GONE);
        setList();
    }

    public void LogOutClick(View view) {
        Log.d(TAG, "onClick: logOut");
        user = null;
        mAuth.signOut();
        finish();
        redirectActivity(MyWardrobe.this, MainActivity.class);
    }

    public void LogInClick(View view) {
        Log.d(TAG, "onClick: logIn");
        finish();
        redirectActivity(MyWardrobe.this, Login.class);
    }

    public void SignUpClick(View view) {
        Log.d(TAG, "onClick: signUp");
        finish();
        redirectActivity(MyWardrobe.this, Register.class);
    }

    private static void redirectActivity(Activity activity, Class<?> aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
    }


    public void setList() {
        myProducts = new ArrayList<>();
        String userId = mAuth.getUid();
        assert userId != null;
        mDatabaseReference.child(USERS).child(userId).child(myWARDROBE).addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot ds : snapshot.getChildren()) {
                        final Product readProduct = ds.getValue(Product.class);

                        assert readProduct != null;
                        if (!myProducts.contains(readProduct))
                            myProducts.add(readProduct);
                    }
                }
                if (myProducts.size() != 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    warning.setVisibility(View.GONE);
                    initList(myProducts);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    warning.setVisibility(View.VISIBLE);
                    warning.setText("No Products");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void initList(ArrayList<Product> category) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(recyclerView);
        MyWardrobeAdapter adapter = new MyWardrobeAdapter(MyWardrobe.this, category, this);
        recyclerView.setAdapter(adapter);
    }

    private void initProductList(ArrayList<Product> category) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(recyclerView);
        ProductsAdapter adapter = new ProductsAdapter(MyWardrobe.this, category, this, TAG);
        recyclerView.setAdapter(adapter);
    }

    private void initializeById() {
        recyclerView = findViewById(R.id.list_combines);
        drawerLayout = findViewById(R.id.drawer_layout);
        relativeFilter = findViewById(R.id.relative_filter);
        relativeMenu = findViewById(R.id.relative_menu);
        recyclerView = findViewById(R.id.list_combines);
        editMin = findViewById(R.id.edit_min);
        editMax = findViewById(R.id.edit_max);
        mangoCheck = findViewById(R.id.check_brand1);
        hmCheck = findViewById(R.id.check_brand2);
        twistCheck = findViewById(R.id.check_brand3);
        bershkaCheck = findViewById(R.id.check_brand4);
        ipekyolCheck = findViewById(R.id.check_brand5);
        hatCheck = findViewById(R.id.check_category1);
        glassesCheck = findViewById(R.id.check_category2);
        dressCheck = findViewById(R.id.check_category3);
        upperCheck = findViewById(R.id.check_category4);
        bottomCheck = findViewById(R.id.check_category5);
        bagCheck = findViewById(R.id.check_category6);
        shoesCheck = findViewById(R.id.check_category7);
        addProductWardrobe = findViewById(R.id.image_add_product);
        addProductWardrobe.setVisibility(View.VISIBLE);
        warning = findViewById(R.id.text_warning);
        backWardrobe = findViewById(R.id.text_back_wardrobe);
        backWardrobe.setVisibility(View.INVISIBLE);
        relativeDrawer = findViewById(R.id.relative_drawer);
        myWardrobeCheck = findViewById(R.id.check_wardrobe);
        TextView textUser = findViewById(R.id.text_user);
        TextView title = findViewById(R.id.text_title);
        title.setText(TITLE);
        if (user != null)
            textUser.setText(user.getFullName());
    }

    @SuppressLint("SetTextI18n")
    private void categorizeProduct() throws JSONException {
        currentList = new ArrayList<>();
        String filterBrands = (String) filterJSON.get("brandFilter");
        String filterMinPrice = (String) filterJSON.get("min");
        String filterMaxPrice = (String) filterJSON.get("max");
        String filterCategory = (String) filterJSON.get("categoryFilter");
        if (filterBrands.equals("") && filterMinPrice.equals("") && filterMaxPrice.equals("") && filterCategory.equals("") && myProducts.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);
            warning.setVisibility(View.GONE);
            initList(myProducts);
        } else {
            for (Product product : myProducts) {
                String price = product.getPrice().replace(".", "");
                price = price.replace(',', '.');
                price = price.replace('₺', ' ');
                price = price.replace("TL", " ");
                if ((filterBrands.equals("") || filterBrands.contains(product.getBrand()))
                        && (filterMinPrice.isEmpty() || Double.parseDouble(filterMinPrice) < Double.parseDouble(price))
                        && (filterMaxPrice.isEmpty() || Double.parseDouble(filterMaxPrice) > Double.parseDouble(price))
                        && (filterCategory.equals("") || filterCategory.contains(product.getFilterType().toString())))
                    currentList.add(product);
            }
            if (currentList.size() != 0) {
                recyclerView.setVisibility(View.VISIBLE);
                warning.setVisibility(View.GONE);
                initList(currentList);
            } else {
                recyclerView.setVisibility(View.GONE);
                warning.setVisibility(View.VISIBLE);
                warning.setText("No Products");
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void categorizeProductWardrobe() throws JSONException {
        currentList = new ArrayList<>();
        String filterBrands = (String) filterJSON.get("brandFilter");
        String filterMinPrice = (String) filterJSON.get("min");
        String filterMaxPrice = (String) filterJSON.get("max");
        String filterCategory = (String) filterJSON.get("categoryFilter");
        for (Product product : products) {
            String price = product.getPrice().replace(".", "");
            price = price.replace(',', '.');
            price = price.replace('₺', ' ');
            price = price.replace("TL", " ");
            if (myWardrobeCheck.isChecked()) {
                for (Product p : myProducts) {
                    if (p.getId().equals(product.getId()))
                        currentList.add(product);
                }
            } else {
                if ((filterBrands.equals("") || filterBrands.contains(product.getBrand()))
                        && (filterMinPrice.isEmpty() || Double.parseDouble(filterMinPrice) < Double.parseDouble(price))
                        && (filterMaxPrice.isEmpty() || Double.parseDouble(filterMaxPrice) > Double.parseDouble(price))
                        && (filterCategory.equals("") || filterCategory.contains(product.getFilterType().toString())))
                    currentList.add(product);
            }
        }
        if (currentList.size() != 0) {
            warning.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            initProductList(currentList);
        } else {
            recyclerView.setVisibility(View.GONE);
            warning.setVisibility(View.VISIBLE);
            warning.setText("No Products");
        }
    }

    private void getProductStartScreen() {
        relativeFilter.bringToFront();
        relativeFilter.setVisibility(View.VISIBLE);
        relativeMenu.setVisibility(View.GONE);
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        int screenWidth = size.x;
        ViewGroup.LayoutParams params = relativeDrawer.getLayoutParams();
        RelativeLayout fashionBox = findViewById(R.id.fashion_section);
        RelativeLayout ratingsBox = findViewById(R.id.ratings_box);
        params.width = screenWidth;
        relativeDrawer.setLayoutParams(params);
        openDrawer(drawerLayout);
        fashionBox.setVisibility(View.GONE);
        ratingsBox.setVisibility(View.GONE);
    }

    @Override
    public void onProductClick(int position) {
        if (currentList.size() == 0) {
            Product product = myProducts.get(position);
            Intent myIntent = new Intent(this, ProductDetail.class);
            myIntent.putExtra("Product", product);
            startActivity(myIntent);
            Log.d(TAG, "clicked" + position);
        } else {
            Product product = currentList.get(position);
            Intent myIntent = new Intent(this, ProductDetail.class);
            myIntent.putExtra("Product", product);
            startActivity(myIntent);
            Log.d(TAG, "clicked" + position);
        }
    }

    private void resetFilter() {
        editMin.setText("");
        editMax.setText("");
        mangoCheck.setChecked(false);
        hmCheck.setChecked(false);
        twistCheck.setChecked(false);
        bershkaCheck.setChecked(false);
        ipekyolCheck.setChecked(false);
        hatCheck.setChecked(false);
        glassesCheck.setChecked(false);
        dressCheck.setChecked(false);
        upperCheck.setChecked(false);
        bottomCheck.setChecked(false);
        bagCheck.setChecked(false);
        shoesCheck.setChecked(false);
        myWardrobeCheck.setChecked(false);
        try {
            filterJSON.put("brandFilter", "");
            filterJSON.put("min", "");
            filterJSON.put("max", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
