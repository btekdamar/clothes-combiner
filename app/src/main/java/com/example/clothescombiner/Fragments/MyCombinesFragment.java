package com.example.clothescombiner.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.clothescombiner.Activities.CombineDetails;
import com.example.clothescombiner.Adapters.MyCombinesAdapter;
import com.example.clothescombiner.Core.Combine;
import com.example.clothescombiner.R;

import java.util.ArrayList;

public class MyCombinesFragment extends Fragment implements MyCombinesAdapter.OnCombineListener {
    private static final String TAG = "MyCombinesFragment";

    private RecyclerView recyclerView;
    Context mContext;

    private ArrayList<Combine> combines;
    private boolean control;

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = rootView.getContext();
        recyclerView = rootView.findViewById(R.id.list_combines);
        combines = (ArrayList<Combine>) getArguments().getSerializable("temp");
        control = getArguments().getBoolean("control");
        if (combines.size() == 0) {
            TextView warning = rootView.findViewById(R.id.text_warning);
            warning.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        else {
            TextView warning = rootView.findViewById(R.id.text_warning);
            warning.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            initList(combines);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initList(ArrayList<Combine> category) {
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(recyclerView);
        MyCombinesAdapter adapter =
                new MyCombinesAdapter(mContext, category, this, control);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCombineClick(int position) {
        Log.d(TAG, "onClick: combineDetails");
        Combine selectedCombine = combines.get(position);
        Intent myIntent = new Intent(mContext, CombineDetails.class);
        myIntent.putExtra("Combine", selectedCombine);
        getActivity().finish();
        startActivity(myIntent);
    }
}
