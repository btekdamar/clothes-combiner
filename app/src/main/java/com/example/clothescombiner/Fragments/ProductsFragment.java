package com.example.clothescombiner.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.clothescombiner.Activities.ProductDetail;
import com.example.clothescombiner.Adapters.ProductsAdapter;
import com.example.clothescombiner.Core.Product;
import com.example.clothescombiner.R;

import java.util.ArrayList;

public class ProductsFragment extends Fragment implements ProductsAdapter.OnProductListener{
    private static final String TAG = "ProductsFragment";

    private RecyclerView recyclerView;
    private String screenTag;
    Context mContext;

    private ArrayList<Product> products;

    @SuppressLint("SetTextI18n")
    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = rootView.getContext();
        recyclerView = rootView.findViewById(R.id.list_combines);
        products = (ArrayList<Product>) getArguments().getSerializable("temp");
        screenTag = getArguments().getString("screenTag");

        if (products.size() == 0) {
            TextView warning = rootView.findViewById(R.id.text_warning);
            warning.setVisibility(View.VISIBLE);
            warning.setText("No Products");
            recyclerView.setVisibility(View.GONE);
        }

        else {
            TextView warning = rootView.findViewById(R.id.text_warning);
            warning.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            initList(products);
        }
        return rootView;
    }

    private void initList(ArrayList<Product> category) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(recyclerView);
        ProductsAdapter adapter = new ProductsAdapter(mContext, category, this, screenTag);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onProductClick(int position) {
        Product product = products.get(position);
        Intent myIntent = new Intent(mContext, ProductDetail.class);
        myIntent.putExtra("Product", product);
        getActivity().finish();
        startActivity(myIntent);
        Log.d(TAG, "clicked" + position);
    }
}
